-- phpMyAdmin SQL Dump
-- version 4.0.10
-- http://www.phpmyadmin.net
--
-- Хост: 127.0.0.1:3306
-- Время создания: Сен 03 2014 г., 14:43
-- Версия сервера: 5.5.38-log
-- Версия PHP: 5.3.26

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- База данных: `work`
--

-- --------------------------------------------------------

--
-- Структура таблицы `app`
--

CREATE TABLE IF NOT EXISTS `app` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `value` varchar(255) NOT NULL,
  `description` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Дамп данных таблицы `app`
--

INSERT INTO `app` (`id`, `name`, `value`, `description`) VALUES
(1, 'auto_send_mail', '1', NULL);

-- --------------------------------------------------------

--
-- Структура таблицы `attachment`
--

CREATE TABLE IF NOT EXISTS `attachment` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `comment_id` int(11) DEFAULT NULL,
  `repair_id` int(11) DEFAULT NULL,
  `part_id` int(11) DEFAULT NULL,
  `attachment` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Структура таблицы `brand`
--

CREATE TABLE IF NOT EXISTS `brand` (
  `brand_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `description` text,
  PRIMARY KEY (`brand_id`),
  KEY `brand_id` (`brand_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Дамп данных таблицы `brand`
--

INSERT INTO `brand` (`brand_id`, `name`, `description`) VALUES
(1, 'Asus', 'lalka'),
(3, 'hp', 'lalka');

-- --------------------------------------------------------

--
-- Структура таблицы `colors`
--

CREATE TABLE IF NOT EXISTS `colors` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `color` varchar(6) NOT NULL,
  `status` int(11) NOT NULL,
  `description` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=10 ;

--
-- Дамп данных таблицы `colors`
--

INSERT INTO `colors` (`id`, `color`, `status`, `description`) VALUES
(1, '', 0, 'New order'),
(2, 'ababab', 1, 'Waiting for Diagnostic'),
(3, '12ff00', 2, 'Waiting for customer agreement'),
(4, '79ff6f', 3, 'Sent for repair'),
(5, 'f13558', 4, 'Waiting for spair parts'),
(6, 'a52fd6', 5, 'Spare parts derived'),
(7, '', 6, 'Successfully completed'),
(8, '', 7, 'fail'),
(9, '', 8, 'Canceled by customer');

-- --------------------------------------------------------

--
-- Структура таблицы `message`
--

CREATE TABLE IF NOT EXISTS `message` (
  `message_id` int(11) NOT NULL AUTO_INCREMENT,
  `type` int(8) NOT NULL COMMENT '0 - новый комментарий:  1 - смена статуса ремонта: 2 - новый пользователь 3 - новый ремонт 4 - ',
  `from_id` int(11) NOT NULL,
  `to_id` int(11) NOT NULL,
  `repair_id` int(11) DEFAULT NULL,
  `message` text NOT NULL,
  `new` int(2) NOT NULL DEFAULT '1',
  `hidden` int(2) NOT NULL DEFAULT '0',
  `date` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`message_id`),
  KEY `from_id` (`from_id`,`to_id`,`repair_id`),
  KEY `to_id` (`to_id`),
  KEY `repair_id` (`repair_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=11 ;

--
-- Дамп данных таблицы `message`
--

INSERT INTO `message` (`message_id`, `type`, `from_id`, `to_id`, `repair_id`, `message`, `new`, `hidden`, `date`) VALUES
(1, 0, 5, -1, 18, 'New repair fromuser', 1, 0, '2014-09-01 08:34:39'),
(2, 0, 7, -1, NULL, 'New user alex', 1, 0, '2014-09-01 09:10:34'),
(3, 2, 9, -1, NULL, 'New user alexxx2141', 1, 0, '2014-09-01 11:13:23'),
(4, 3, 5, -1, 19, 'New repair from user', 1, 0, '2014-09-01 18:24:41'),
(5, 3, 5, -1, 20, 'New repair from user', 1, 0, '2014-09-01 20:23:26'),
(6, 3, 5, -1, 21, 'New repair from user', 1, 0, '2014-09-01 20:25:07'),
(7, 2, 10, -1, NULL, 'New user test', 1, 0, '2014-09-02 15:12:00'),
(8, 2, 11, -1, NULL, 'New user adada', 1, 0, '2014-09-02 15:21:41'),
(9, 2, 12, -1, NULL, 'New user dsaasd', 1, 0, '2014-09-02 15:22:42'),
(10, 2, 13, -1, NULL, 'New user 123', 1, 0, '2014-09-02 15:24:03');

-- --------------------------------------------------------

--
-- Структура таблицы `repair`
--

CREATE TABLE IF NOT EXISTS `repair` (
  `repair_id` int(11) NOT NULL AUTO_INCREMENT,
  `brand_id` int(11) NOT NULL,
  `type` int(2) DEFAULT '0',
  `model` varchar(255) NOT NULL,
  `accessories` varchar(255) NOT NULL,
  `send_method` int(8) NOT NULL,
  `old_service_pswd` varchar(255) DEFAULT NULL,
  `tracking_number` varchar(255) DEFAULT NULL,
  `shiping_number` varchar(255) DEFAULT NULL,
  `damage` text NOT NULL,
  `diagnostic` text,
  `date` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `client_id` int(11) NOT NULL,
  `support_id` int(11) DEFAULT NULL COMMENT 'кто ремонтирует',
  `status` int(11) DEFAULT '0' COMMENT '0. Новый заказ, 1. Подтверждение заказа 2. Диагностика 3. Подтверждение заказчиком 4. Направлено на ремонт 5. Ожидание комплектующих 6. Получение комплектующих',
  `warehouse_id` int(255) DEFAULT NULL,
  `price` float(255,2) DEFAULT '0.00',
  PRIMARY KEY (`repair_id`),
  KEY `brand_id` (`brand_id`,`client_id`,`support_id`),
  KEY `client_id` (`client_id`),
  KEY `id` (`support_id`),
  KEY `send_method` (`send_method`),
  KEY `client_id_2` (`client_id`),
  KEY `warehouse_id` (`warehouse_id`),
  KEY `status` (`status`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=22 ;

--
-- Дамп данных таблицы `repair`
--

INSERT INTO `repair` (`repair_id`, `brand_id`, `type`, `model`, `accessories`, `send_method`, `old_service_pswd`, `tracking_number`, `shiping_number`, `damage`, `diagnostic`, `date`, `client_id`, `support_id`, `status`, `warehouse_id`, `price`) VALUES
(1, 1, 0, 'asd', 'asd', 1, '', '', '', 'asd', NULL, '0000-00-00 00:00:00', 1, NULL, 0, 0, 0.00),
(2, 0, 0, 'asd', '', 0, '123', '123', 'asd', 'asdasddsasaddaads', NULL, '2014-08-28 13:29:48', 5, NULL, 0, 0, 0.00),
(3, 1, 0, 'asd', '', 0, '123', '123', 'asd', 'asdasddsasaddaads', NULL, '2014-08-28 13:30:17', 5, 3, 6, 0, 0.00),
(4, 1, 0, '111', '', 0, '11', '11', '1', '111', NULL, '2014-08-28 13:30:57', 1, 1, 1, 0, 0.00),
(5, 1, 0, '111', '', 0, '11', '11', '1', '111', NULL, '2014-08-28 13:31:01', 1, 1, 2, 0, 0.00),
(6, 1, 0, '111', '', 0, '11', '11', '1', '111', NULL, '2014-08-28 13:30:57', 1, 1, 6, 0, 0.00),
(16, 1, 0, 'qwrqrw', 'saasd', 1, '', '', '', 'saddasdas', NULL, '2014-08-31 15:02:40', 5, NULL, 2, NULL, 0.00),
(17, 1, 0, 'test', 'adsasd', 1, '', '', '', 'nope', NULL, '2014-09-01 08:33:46', 5, NULL, 0, NULL, 0.00),
(18, 1, 0, 'test', 'adsasd', 1, '', '', '', 'nope', NULL, '2014-09-01 08:34:39', 5, NULL, 0, NULL, 0.00),
(19, 1, 0, 'testssssssss', 'testssssssss', 1, 'testssssssss', '', '', 'testssssssss', NULL, '2014-09-01 18:24:41', 5, NULL, 0, NULL, 0.00),
(20, 1, 0, 'ewewewe', 'Battery,Power_supply,tests, hello', 1, '', '', '', 'qweqqewqe', NULL, '2014-09-01 20:23:26', 5, NULL, 0, NULL, 0.00),
(21, 1, 0, 'nope', 'Battery,Power_supply,Test2', 1, '', '', '', 'qewqeweqweq', NULL, '2014-09-01 20:25:07', 5, NULL, 0, NULL, 0.00);

-- --------------------------------------------------------

--
-- Структура таблицы `send_method`
--

CREATE TABLE IF NOT EXISTS `send_method` (
  `method_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `description` text,
  PRIMARY KEY (`method_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Дамп данных таблицы `send_method`
--

INSERT INTO `send_method` (`method_id`, `name`, `description`) VALUES
(1, 'pochta', 'huechta'),
(2, 'airplan', 'narkoman :D');

-- --------------------------------------------------------

--
-- Структура таблицы `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `first_name` varchar(255) NOT NULL,
  `last_name` varchar(255) NOT NULL,
  `address` varchar(255) NOT NULL,
  `city` varchar(255) NOT NULL,
  `phone_numb` varchar(255) NOT NULL,
  `cell_phone` varchar(255) NOT NULL,
  `company_name` varchar(255) NOT NULL,
  `profession` varchar(255) NOT NULL,
  `tax_agency` varchar(255) NOT NULL,
  `vat_number` varchar(255) NOT NULL,
  `payment_method` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `login` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `active` int(10) NOT NULL DEFAULT '2',
  `role_id` int(11) NOT NULL DEFAULT '0',
  `comment` varchar(255) NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=14 ;

--
-- Дамп данных таблицы `users`
--

INSERT INTO `users` (`id`, `first_name`, `last_name`, `address`, `city`, `phone_numb`, `cell_phone`, `company_name`, `profession`, `tax_agency`, `vat_number`, `payment_method`, `email`, `login`, `password`, `active`, `role_id`, `comment`, `date`) VALUES
(1, 'Lamo', 'Lamo', ' shachtar 20, 101', 'Makeevko', '+380505440144', '+380505440144', 'Pizdaboliya', 'pizdabol', 'pizdabol express', 'P13D480L', 'Hatypoi', 'FreelancerMOL@gmail.com', 'freeshka', '123', 0, 0, 'Kak yeby suka', '2014-08-31 15:40:37'),
(3, '', '', '', '', '', '', '', '', '', '', '', '', 'moder', 'moder', 1, 1, 'moder', '2014-08-28 13:20:10'),
(4, '', '', '', '', '', '', '', '', '', '', '', '', 'admin', 'admin', 1, 2, '', '2014-08-28 13:20:23'),
(5, '', '', 'asd', '', '', '', '', '', '', '', '', '', 'user', 'user', 1, 0, '', '2014-09-01 08:33:11'),
(6, '', '', '', '', '', '', '', '', '', '', '', '', 'lalka', 'lalka', 2, 2, '', '2014-08-31 15:40:40'),
(7, 'Alex', 'Lex', 'Kyiv ', '13123', '123123123', '123132132', 'dno', 'wat', 'vat', '12', '123', 'bbwfizudlntc@dropmail.me', 'alex', 'alex', 2, 0, 'nahuya?', '2014-09-01 09:24:50'),
(8, 'adsada', 'cfada', 'asdaqd', 'vsgsf', '12313', '12312312', 'asddasadasd', 'aada', 'aad', 'ads', 'ad', 'baojnsge@10mail.org', 'alexxx', 'baojnsge@10mail.org', 1, 0, 'baojnsge@10mail.org', '2014-09-01 11:11:03'),
(9, 'adsada', 'cfada', 'asdaqd', 'vsgsf', '12313', '12312312', 'asddasadasd', 'aada', 'aad', 'ads', 'ad', 'baojnsge@10mail.org', 'alexxx2141', 'baojnsge@10mail.org', 2, 0, 'baojnsge@10mail.org', '2014-09-01 11:13:22'),
(11, 'test', 'asaf', 'affa', 'adfsfas', 'eadsf`', '132', '412', '412', '325', '124', '142', 'test@otes', 'adada', '123', 2, 0, '', '2014-09-02 15:21:41'),
(12, 'asdasads', '21142', '215rwq', '1243ed', '124', '14', '41', '151', '21532', 'lk', 'lklk', 'test@as', 'dsaasd', '123', 3, 0, '', '2014-09-02 15:22:42'),
(13, 'alexas', 'lex13', 'nope', '1234', '1234', '1234', '1234', '1234', '1234', '1234', '1234', 'mad@mail.com', '123', '1234', 3, 0, '', '2014-09-02 15:24:02');

-- --------------------------------------------------------

--
-- Структура таблицы `warehouse`
--

CREATE TABLE IF NOT EXISTS `warehouse` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `number` int(255) NOT NULL,
  `status` int(2) NOT NULL COMMENT '0 - empty;1 full',
  `description` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=301 ;

--
-- Дамп данных таблицы `warehouse`
--

INSERT INTO `warehouse` (`id`, `number`, `status`, `description`) VALUES
(1, 1, 0, 0),
(2, 2, 0, 0),
(3, 3, 0, 0),
(4, 4, 0, 0),
(5, 5, 0, 0),
(6, 6, 0, 0),
(7, 7, 0, 0),
(8, 8, 0, 0),
(9, 9, 0, 0),
(10, 10, 0, 0),
(11, 11, 0, 0),
(12, 12, 0, 0),
(13, 13, 0, 0),
(14, 14, 0, 0),
(15, 15, 0, 0),
(16, 16, 0, 0),
(17, 17, 0, 0),
(18, 18, 0, 0),
(19, 19, 0, 0),
(20, 20, 0, 0),
(21, 21, 0, 0),
(22, 22, 0, 0),
(23, 23, 0, 0),
(24, 24, 0, 0),
(25, 25, 0, 0),
(26, 26, 0, 0),
(27, 27, 0, 0),
(28, 28, 0, 0),
(29, 29, 0, 0),
(30, 30, 0, 0),
(31, 31, 0, 0),
(32, 32, 0, 0),
(33, 33, 0, 0),
(34, 34, 0, 0),
(35, 35, 0, 0),
(36, 36, 0, 0),
(37, 37, 0, 0),
(38, 38, 0, 0),
(39, 39, 0, 0),
(40, 40, 0, 0),
(41, 41, 0, 0),
(42, 42, 0, 0),
(43, 43, 0, 0),
(44, 44, 0, 0),
(45, 45, 0, 0),
(46, 46, 0, 0),
(47, 47, 0, 0),
(48, 48, 0, 0),
(49, 49, 0, 0),
(50, 50, 0, 0),
(51, 51, 0, 0),
(52, 52, 0, 0),
(53, 53, 0, 0),
(54, 54, 0, 0),
(55, 55, 0, 0),
(56, 56, 0, 0),
(57, 57, 0, 0),
(58, 58, 0, 0),
(59, 59, 0, 0),
(60, 60, 0, 0),
(61, 61, 0, 0),
(62, 62, 0, 0),
(63, 63, 0, 0),
(64, 64, 0, 0),
(65, 65, 0, 0),
(66, 66, 0, 0),
(67, 67, 0, 0),
(68, 68, 0, 0),
(69, 69, 0, 0),
(70, 70, 0, 0),
(71, 71, 0, 0),
(72, 72, 0, 0),
(73, 73, 0, 0),
(74, 74, 0, 0),
(75, 75, 0, 0),
(76, 76, 0, 0),
(77, 77, 0, 0),
(78, 78, 0, 0),
(79, 79, 0, 0),
(80, 80, 0, 0),
(81, 81, 0, 0),
(82, 82, 0, 0),
(83, 83, 0, 0),
(84, 84, 0, 0),
(85, 85, 0, 0),
(86, 86, 0, 0),
(87, 87, 0, 0),
(88, 88, 0, 0),
(89, 89, 0, 0),
(90, 90, 0, 0),
(91, 91, 0, 0),
(92, 92, 0, 0),
(93, 93, 0, 0),
(94, 94, 0, 0),
(95, 95, 0, 0),
(96, 96, 0, 0),
(97, 97, 0, 0),
(98, 98, 0, 0),
(99, 99, 0, 0),
(100, 100, 0, 0),
(101, 101, 0, 0),
(102, 102, 0, 0),
(103, 103, 0, 0),
(104, 104, 0, 0),
(105, 105, 0, 0),
(106, 106, 0, 0),
(107, 107, 0, 0),
(108, 108, 0, 0),
(109, 109, 0, 0),
(110, 110, 0, 0),
(111, 111, 0, 0),
(112, 112, 0, 0),
(113, 113, 0, 0),
(114, 114, 0, 0),
(115, 115, 0, 0),
(116, 116, 0, 0),
(117, 117, 0, 0),
(118, 118, 0, 0),
(119, 119, 0, 0),
(120, 120, 0, 0),
(121, 121, 0, 0),
(122, 122, 0, 0),
(123, 123, 0, 0),
(124, 124, 0, 0),
(125, 125, 0, 0),
(126, 126, 0, 0),
(127, 127, 0, 0),
(128, 128, 0, 0),
(129, 129, 0, 0),
(130, 130, 0, 0),
(131, 131, 0, 0),
(132, 132, 0, 0),
(133, 133, 0, 0),
(134, 134, 0, 0),
(135, 135, 0, 0),
(136, 136, 0, 0),
(137, 137, 0, 0),
(138, 138, 0, 0),
(139, 139, 0, 0),
(140, 140, 0, 0),
(141, 141, 0, 0),
(142, 142, 0, 0),
(143, 143, 0, 0),
(144, 144, 0, 0),
(145, 145, 0, 0),
(146, 146, 0, 0),
(147, 147, 0, 0),
(148, 148, 0, 0),
(149, 149, 0, 0),
(150, 150, 0, 0),
(151, 151, 0, 0),
(152, 152, 0, 0),
(153, 153, 0, 0),
(154, 154, 0, 0),
(155, 155, 0, 0),
(156, 156, 0, 0),
(157, 157, 0, 0),
(158, 158, 0, 0),
(159, 159, 0, 0),
(160, 160, 0, 0),
(161, 161, 0, 0),
(162, 162, 0, 0),
(163, 163, 0, 0),
(164, 164, 0, 0),
(165, 165, 0, 0),
(166, 166, 0, 0),
(167, 167, 0, 0),
(168, 168, 0, 0),
(169, 169, 0, 0),
(170, 170, 0, 0),
(171, 171, 0, 0),
(172, 172, 0, 0),
(173, 173, 0, 0),
(174, 174, 0, 0),
(175, 175, 0, 0),
(176, 176, 0, 0),
(177, 177, 0, 0),
(178, 178, 0, 0),
(179, 179, 0, 0),
(180, 180, 0, 0),
(181, 181, 0, 0),
(182, 182, 0, 0),
(183, 183, 0, 0),
(184, 184, 0, 0),
(185, 185, 0, 0),
(186, 186, 0, 0),
(187, 187, 0, 0),
(188, 188, 0, 0),
(189, 189, 0, 0),
(190, 190, 0, 0),
(191, 191, 0, 0),
(192, 192, 0, 0),
(193, 193, 0, 0),
(194, 194, 0, 0),
(195, 195, 0, 0),
(196, 196, 0, 0),
(197, 197, 0, 0),
(198, 198, 0, 0),
(199, 199, 0, 0),
(200, 200, 0, 0),
(201, 201, 0, 0),
(202, 202, 0, 0),
(203, 203, 0, 0),
(204, 204, 0, 0),
(205, 205, 0, 0),
(206, 206, 0, 0),
(207, 207, 0, 0),
(208, 208, 0, 0),
(209, 209, 0, 0),
(210, 210, 0, 0),
(211, 211, 0, 0),
(212, 212, 0, 0),
(213, 213, 0, 0),
(214, 214, 0, 0),
(215, 215, 0, 0),
(216, 216, 0, 0),
(217, 217, 0, 0),
(218, 218, 0, 0),
(219, 219, 0, 0),
(220, 220, 0, 0),
(221, 221, 0, 0),
(222, 222, 0, 0),
(223, 223, 0, 0),
(224, 224, 0, 0),
(225, 225, 0, 0),
(226, 226, 0, 0),
(227, 227, 0, 0),
(228, 228, 0, 0),
(229, 229, 0, 0),
(230, 230, 0, 0),
(231, 231, 0, 0),
(232, 232, 0, 0),
(233, 233, 0, 0),
(234, 234, 0, 0),
(235, 235, 0, 0),
(236, 236, 0, 0),
(237, 237, 0, 0),
(238, 238, 0, 0),
(239, 239, 0, 0),
(240, 240, 0, 0),
(241, 241, 0, 0),
(242, 242, 0, 0),
(243, 243, 0, 0),
(244, 244, 0, 0),
(245, 245, 0, 0),
(246, 246, 0, 0),
(247, 247, 0, 0),
(248, 248, 0, 0),
(249, 249, 0, 0),
(250, 250, 0, 0),
(251, 251, 0, 0),
(252, 252, 0, 0),
(253, 253, 0, 0),
(254, 254, 0, 0),
(255, 255, 0, 0),
(256, 256, 0, 0),
(257, 257, 0, 0),
(258, 258, 0, 0),
(259, 259, 0, 0),
(260, 260, 0, 0),
(261, 261, 0, 0),
(262, 262, 0, 0),
(263, 263, 0, 0),
(264, 264, 0, 0),
(265, 265, 0, 0),
(266, 266, 0, 0),
(267, 267, 0, 0),
(268, 268, 0, 0),
(269, 269, 0, 0),
(270, 270, 0, 0),
(271, 271, 0, 0),
(272, 272, 0, 0),
(273, 273, 0, 0),
(274, 274, 0, 0),
(275, 275, 0, 0),
(276, 276, 0, 0),
(277, 277, 0, 0),
(278, 278, 0, 0),
(279, 279, 0, 0),
(280, 280, 0, 0),
(281, 281, 0, 0),
(282, 282, 0, 0),
(283, 283, 0, 0),
(284, 284, 0, 0),
(285, 285, 0, 0),
(286, 286, 0, 0),
(287, 287, 0, 0),
(288, 288, 0, 0),
(289, 289, 0, 0),
(290, 290, 0, 0),
(291, 291, 0, 0),
(292, 292, 0, 0),
(293, 293, 0, 0),
(294, 294, 0, 0),
(295, 295, 0, 0),
(296, 296, 0, 0),
(297, 297, 0, 0),
(298, 298, 0, 0),
(299, 299, 0, 0),
(300, 300, 0, 0);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
