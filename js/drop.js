
	var dropZone = $('#dropZone'),
		maxFileSize = 1000000;

	// Проверка поддержки браузером
	if (typeof(window.FileReader) == 'undefined') {
		dropZone.text('Не поддерживается браузером!');
		dropZone.addClass('error');
	}

	// Добавляем класс hover при наведении
	dropZone[0].ondragover = function() {
		dropZone.addClass('hover');
		return false;
	};

	// Убираем класс hover
	dropZone[0].ondragleave = function() {
		dropZone.removeClass('hover');
		return false;
	};

	// Обрабатываем событие Drop
	dropZone[0].ondrop = function(event) {
		event.preventDefault();
		dropZone.removeClass('hover');
		dropZone.addClass('drop');

		var file = event.dataTransfer.files[0];

		// Проверяем размер файла
		if (file.size > maxFileSize) {
			dropZone.text('Файл слишком большой!');
			dropZone.addClass('error');
			return false;
		}

		// Конвертируем
		console.log(file);
		var reader = new FileReader();
		reader.onload = function(event) {
			var dataUri = event.target.result;
			$('.attachd').fadeOut('fast');
			$('.attachd').before('<img src="'+dataUri+'" height="100px" style="margin:10px;"/>').fadeIn('slow');
			$('.attachment-img').val(dataUri);
			dropZone.fadeOut('slow');
		};

		reader.onerror = function(event) {
			console.error("Файл не может быть прочитан! код " + event.target.error.code);
		};

		reader.readAsDataURL(file);

	};

	// Показываем процент загрузки
	function uploadProgress(event) {
		var percent = parseInt(event.loaded / event.total * 100);
		dropZone.text('Загрузка: ' + percent + '%');
	}

	// Пост обрабочик
	function stateChange(event) {
		if (event.target.readyState == 4) {
			if (event.target.status == 200) {
				dropZone.text('Загрузка успешно завершена!');
			} else {
				dropZone.text('Произошла ошибка!');
				dropZone.addClass('error');
			}
		}
	}

	function convertImgToBase64(imgElem){
		var reader = new FileReader();
		reader.onload = function(imgElem){
			var binaryString = imgElem.target.result;
			return btoa(binaryString);
		}

	}
