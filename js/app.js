/**
 * Created by root on 22.04.14.
 */

$(document).ready(function () {

	setInterval(function(){
		if($('#action_id').val()!='new'&&$('#action_id').val()!='addparts'&&($('#action_id').val()!='index'&&$('controller_id').val()!='user'))
		window.location.reload();
	},600000)

	$('.accessories_list').click(function () {
		$('.accessories').val('');


		$(".accessories_list option:selected").each(function () {
			if ($(this).val() == 'Other') {
				$('.other_accessories').fadeIn();
			} else {
				$('.other_accessories').fadeOut();
			}
		});
		$('.accessories').val('');

		$(".accessories_list option:selected").each(function () {

			if ($(this).val() != 'Other') {
				$('.accessories').val($('.accessories').val() + $(this).val() + ',');
			}
		});
		$('.accessories').val($('.accessories').val() + $('.other_accessories').val());


	});



	$('.other_accessories').keyup(function(){
		$('.accessories').val('');

		$(".accessories_list option:selected").each(function () {

			if ($(this).val() != 'Other') {
				$('.accessories').val($('.accessories').val() + $(this).val() + ',');
			}
		});
		$('.accessories').val($('.accessories').val() + $('.other_accessories').val());
	});

	var hideme= function(el){
		console.log(el);
	}

	$('.colorpick').colpick({
		colorScheme: 'dark',
		layout: 'rgbhex',
		onShow:function(el){

		},
		onSubmit: function (hsb, hex, rgb, el) {

			/*
			*
			*   ele: 'body', // which element to append to
			 type: 'info', // (null, 'info', 'error', 'success')
			 offset: {from: 'top', amount: 20}, // 'top', or 'bottom'
			 align: 'right', // ('left', 'right', or 'center')
			 width: 250, // (integer, or 'auto')
			 delay: 4000,
			 allow_dismiss: true,
			 stackup_spacing: 10 // spacing between consecutively stacked growls.

			* */
			$.bootstrapGrowl("Wait",{
				offset: {from: 'top', amount: 200},
				align: 'center',
				width: 'auto',
				delay: 50000,
				allow_dismiss: false
				/*
				 ele: 'body', // which element to append to
				 type: 'info', // (null, 'info', 'error', 'success')
				 offset: {from: 'top', amount: 20}, // 'top', or 'bottom'
				 align: 'right', // ('left', 'right', or 'center')
				 width: 250, // (integer, or 'auto')
				 delay: 4000,
				 allow_dismiss: true,
				 stackup_spacing: 10 // spacing between consecutively stacked growls.
				 * */
			});
			$(el).css('background-color', '#' + hex);
			$.ajax(
				{
					type: "POST",
					url: $(el).data('url') + '/?id=' + $(el).data('id') + '&color=' + hex,
					success: function (msg) {
						$.bootstrapGrowl("Changed", {
							offset: {from: 'top', amount: 250},
							delay: 50000,
							type: 'success',
							align: 'center',
							width: 'auto',
							allow_dismiss: false
						});
					},
					error: function (dat) {

						var message = "<div id=\"notification\" align=\"center\" onclick='hideme(this)'>"+
							"<a id=\"cc\"><div class=\"row\"><div class=\"back btn btn-success\" id=\"fade-out\">" +
							dat+
							"</div></div></a>" +
							"</div>";
						$('.main').before(message);
					}
				}
			);
			$(el).colpickHide();
		}
	});



	var comments;
	$(document).on('click','#print_me',function () {
		comments = $('.inner_comments').html();
		$('#itisheader').remove();
		$('.inner_comments').remove();
		$('.user_comments').addClass('col-sm-12').removeClass('col-sm-6');
		var pdf = new jsPDF('p', 'pt', 'a4');

		pdf.addHTML(document.body, function () {
			var string = pdf.output('datauristring');
			$('#print_me').html('Download PDF').attr('src', string).attr('id', 'download-pdf').attr('href', $('#download-pdf').attr('src')).attr('target', '_blank');
		});


	});

	$(document).on('click','#download-pdf',function(){


		window.location.reload();
	})




	setTimeout(function () {
		$('#notification').fadeOut('slow');
	}, 5000);

	$('#a').click(function () {
		$('#notification').fadeOut('slow');
	});

	var select = false;
	var timer;
	$('#usearch').keyup(function(e){
		$('.repair_button').attr('disabled','true').html('Looking for user');
		setTimeout(function(){
		$.ajax({

			url: $('#usearch').data('url') + '?name='+$('#usearch').val()
		}).done(function(dat) {
			var result = dat.split(';');
			var login = result[0];
			var id = result[1];
			if(!select){
			if(result.length==2){
				$('.repair_button').removeAttr('disabled').html('Send');
				$('.usetlist option').each(function(){
					if($(this).val()==id){
						$(this).attr('selected','selected');
					}

				});

			}
			}else{
				$('.repair_button').attr('disabled','true').html('User '+ dat +' not found');
			}
		})},1400);
	});

	$('#Message_to_id').change(function(){
		$('.usetlist option').each(function(){
			if($(this).attr('selected')=='selected'){
				$('#usearch').val($(this).val());
				$('.repair_button').removeAttr('disabled').html('Send');
			}
		});
	});


	/* *
	 #search_text
	 #search_user
	 #users_list

	 $('#i').keyup(function(e){
	 if($('#i').val().length%4==1){
	 $('#test').val($('#test').val() + '|'+ e.keyCode);
	 }
	 //32 space
	 //13 enter
	 //8 back space
	 console.log(e.keyCode);
	 })


	* */
});