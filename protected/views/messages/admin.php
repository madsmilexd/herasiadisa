<div class="col-lg-offset-2 col-sm-8">
    <div class="col-sm-12 reg_container" align="center">
        <div class="reg_header col-sm-12">
            <div class="col-sm-4">MANAGE MESSAGES</div>
            <div class="col-sm-5">
                <?
                $this->widget('bootstrap.widgets.TbButton', array(
                    'label' => 'Create',
                    'url' => Yii::app()->createAbsoluteUrl(Yii::app()->controller->id.'/create'),
                    'htmlOptions' => array('class' => 'menu_button')
                ));
                ?>
            </div>

        </div>
<?php
$this->breadcrumbs=array(
	'Messages'=>array('index'),
	'Manage',
);


?>
        <div class="col-sm-12">



			<table class="items table table-striped table-bordered table-condensed">
				<thead>
				<tr>
					<th scope="col">ID</th>
					<th scope="col">From</th>
					<th scope="col">To</th>
					<th scope="col">Text</th>
					<th scope="col">Open</th>
				</tr>
				</thead>
				<tbody>
				<?
				foreach($model as $mes){
					?>

				<tr <?=($mes->new==1)? 'class="info"':'';?>>
					<td><?=$mes->message_id;?></td>
					<td><?=($mes->from->login)?$mes->from->login:'admin';?></td>
					<td><?=($mes->to->login)?$mes->to->login:'admin';?></td>
					<td><?=$mes->message;?></td>
					<td><?
						$res = Yii::app()->createAbsoluteUrl('messages/create');
							if(Yii::app()->user->id == $mes->from->id){
								$res =Yii::app()->createAbsoluteUrl('messages/chat/'.$mes->to->id);
								}
							if(Yii::app()->user->id == $mes->to->id){
								$res =Yii::app()->createAbsoluteUrl('messages/chat/'.$mes->from->id);
								}
						echo "<a href='$res'>Open chat</a>";
						?></td>
				</tr>
				<?}?>
				</tbody>
			</table>
			<?





//$this->widget('bootstrap.widgets.TbGridView', array(
//	'id'=>'message-grid',
//    'type' => 'striped bordered condensed',
//	'dataProvider'=>$model,
//    'pager' => array(
//        'cssFile'=>'style.css',
//        'maxButtonCount'=>'5',
//        'header' => '', //'<b>Перейти к странице:</b><br><br>', // заголовок над листалкой
//        'prevPageLabel' => 'Prev',
//        'nextPageLabel' => 'Next',
//        'htmlOptions' => array('class' => 'col-sm-12'),
//        'selectedPageCssClass' => 'active'
//    ),
////	'columns'=>array(
////		'type',
////		'from_id',
////		'to_id',
////		'repair_id',
////		'message',
////		/*
////		'new',
////		'hidden',
////		'date',
////		*/
//////		array(
//////			'class'=>'CButtonColumn',
//////		),
////	),
//    'columns'=>array(
//        'message_id',
//		array(
//			'name'=>'to_id',
//			'header'=>'To',
//			'type'=>'raw',
//			'value'=>function($data){
//					if(!$data->to){
//						$res = 'Admin';
//					}else{
//						$res = $data->to->login;
//					}
//					return $res;
//				}
//		),
//        array(
//            'name'=>'from_id',
//            'header'=>'From',
//			'type'=>'raw',
//	        'value'=>function($data){
//					if(!$data->from){
//						$res = 'Admin';
//					}else{
//						$res = $data->from->login;
//					}
//					return $res;
//				}
//        ),
//        'message',
//        'date',
//        array(
//            'name'=>'to_id',
//            'header'=>'Action',
//			'type'=>'raw',
//			'value'=>function($data){
//					$res = Yii::app()->createAbsoluteUrl('messages/create');
//					if(Yii::app()->user->id == $data->from->id){
//						$res =Yii::app()->createAbsoluteUrl('messages/chat/'.$data->to->id);
//					}
//					if(Yii::app()->user->id == $data->to->id){
//						$res =Yii::app()->createAbsoluteUrl('messages/chat/'.$data->from->id);
//					}
//					return "<a href='$res'>Open</a>";
//				}
//        )
//        ),
//
//    ));
?>
            </div>
