<?php $form=$this->beginWidget('CActiveForm', array(
    'id'=>'message-form',
    'enableAjaxValidation'=>false,
)); ?>
<div class="col-lg-offset-2 col-sm-8">
    <div class="col-sm-12 reg_container">
        <div class="reg_header col-sm-12">
            <div class="col-sm-4">CHAT WITH USER</div>
            <div class="col-sm-5">
                <?
                $this->widget('bootstrap.widgets.TbButton', array(
                    'label' => 'Create',
                    'url' => Yii::app()->createAbsoluteUrl(Yii::app()->controller->id.'/create'),
                    'htmlOptions' => array('class' => 'menu_button')
                ));
                ?>
            </div>
            <div class="col-sm-3" align="right">
            </div>
        </div>
        <?
        foreach($model as $message)
        {if($message->type==Message::TYPE_MESSAGE){
            echo '<div class="col-lg-offset-3 col-sm-6">';
            echo '<div class="under_header_header">';
            echo '<div class="col-sm-6" style="padding-left: 15px;">MESSSAGE #';
            echo $message->message_id;

            echo '</div>';
            echo '<div class="col-sm-6" align="right">';
            echo 'Date: ' . $message->date;
            echo '</div></div>';
            echo '<div class="col-sm-5 message no_padding">   From: ';
            echo $message->from->login;

            echo '</div>';
            echo '<div class="col-sm-7 message no_padding" align="right">   To: ' . $message->to->login;
            echo '</div>';
            echo '<div class="comment_body">';
            echo $message->message;
            echo '</div>';
            echo '</div>';
			if(Yii::app()->user->id == $message->to_id){
			$message->new = Message::MESSAGE_OLD;
			$message->save();
			}
        }}

        ?>


        <div class="col-lg-offset-3 col-sm-6" style="padding-top: 25px">
            <div class="col-sm-12 input_label">
                <?=$form->labelEx($model2, 'message'); ?>
            </div>
            <div class="col-sm-12 input_container">
                <?=$form->textArea($model2, 'message', array('class' => 'form-control','rows' => 6)); ?>
            </div>
        <div class="col-lg-offset-4 col-sm-4">
            <?
            $this->widget(
                'bootstrap.widgets.TbButton',
                array('buttonType' => 'submit',
                    'label' => 'Send',
                    'htmlOptions' => array('class' => 'repair_button'))
            );
            ?>
        </div>

        <?php $this->endWidget(); ?>
        </div>
        </div>
