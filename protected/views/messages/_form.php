<?php
/* @var $this MessagesController */
/* @var $model Message */
/* @var $form CActiveForm */
?>

<div class="form col-sm-12">

    <?php $form = $this->beginWidget('CActiveForm', array(
        'id' => 'message-form',
        // Please note: When you enable ajax validation, make sure the corresponding
        // controller action is handling ajax validation correctly.
        // There is a call to performAjaxValidation() commented in generated controller code.
        // See class documentation of CActiveForm for details on this.
        'enableAjaxValidation' => false,
    )); ?>

	<?
	if($model->hasErrors()){
		foreach($model->errors as $r){
			echo $r[0].'<br/>';
		}
	}
	?>

    <div class="col-sm-5 input_label">
        <?= $form->labelEx($model, 'to_id'); ?>
    </div>
    <div class="col-sm-7 input_container">
<!--		--><?//=$form->textField($model,'to_id',array('class'=>'form-control', 'id'=>'acp'));?>
		<input class="form-control col-sm-6" id="usearch" data-url="<?=Yii::app()->createAbsoluteUrl
			('messages/userlist');?>" placeholder="Search"/>
        <?= $form->dropDownList($model, 'to_id', CHtml::listData($users, 'id', 'login'),
			array('class' => 'form-control col-sm-6 usetlist')); ?>
    </div>

    <div class="col-sm-5 input_label">
        <?php echo $form->labelEx($model, 'message'); ?>
    </div>

<div class="col-sm-7 input_container">
    <?php echo $form->textArea($model, 'message', array('class' => 'form-control','rows' => 6)); ?>
</div>
    </div>
<div class="col-lg-offset-3">
    <?
    $this->widget(
        'bootstrap.widgets.TbButton',
        array('buttonType' => 'submit',
            'label' => 'Send',
            'htmlOptions' => array('class' => 'repair_button'))
    );
    ?>
</div>

<?php $this->endWidget(); ?>

<?
/*
 * смотри про автокомплит тут
 * http://www.linkexchanger.su/2008/39.html
 */
?>
