<div class="col-md-offset-2 col-sm-8 col-md-offset-2">
    <div class="col-sm-12 reg_container">
        <div class="reg_header col-sm-12">
            <div class="col-sm-6">MANAGE COLORS</div>
            <div class="col-sm-6" align="right">

            </div>
        </div>
<?php
/* @var $model Colors */

$baseUrl = Yii::app()->baseUrl;
$cs = Yii::app()->getClientScript();
$cs->registerScriptFile($baseUrl . '/vendors/colorpicker/js/colorpicker.js');
$cs->registerCssFile($baseUrl . '/vendors/colorpicker/css/colorpicker.css');
?>


<?php $this->widget('bootstrap.widgets.TbGridView', array(
    'id'=>'brand-grid',
    'type' => 'striped bordered condensed',
	'dataProvider' => $model->search(),
	'columns' => array(
		array(
			'name' => 'status',
			'type' => 'raw',
			'value' => function ($data) {
					return Repair::getStatus($data->status);
				}
		),
		array(
			'name' => 'color',
			'type' => 'raw',
			'value' => function ($data) {

					return '<div data-id="'.$data->id.'" data-color="'.$data->color.'" data-url="'.Yii::app()
					->createAbsoluteUrl
						('colors/update').'"
					class="colorpick"
					style="background:#'
					.$data->color
					.';
					height:40px;
					width:40px;
					border:1px solid #c0c0c0;
					"></div>#'.$data->color;

				},
		),
		array(
			'name' => 'description',
			'type' => 'raw',
			'value' => function ($data) {
					return $data->description;
				}
		),
		array(
			'header' => '',
			'class' => 'CButtonColumn',
			'template' => '{update}',
			'buttons' => array
			(
				'update' => array
				(
//					'name'=>'color',
					'imageUrl' => false,
					'label' => '<span class="glyphicon glyphicon-pencil"></span>',
					'options' => array('title' => 'edit', 'class' => 'text-center'),
//					'url'=> function($data){
//							return 'colorupdates'.$data->$id;
//						}
				),
			),
		),
	),
)); ?>
        </div></div>
