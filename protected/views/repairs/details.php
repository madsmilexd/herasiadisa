<?
/** @var Repair $model */
?>
<div class="col-md-offset-2 col-sm-8 col-md-offset-2">
    <div class="col-sm-12 reg_container">
        <div class="reg_header col-sm-12">
			<?$d = explode('/',$model->date,1);
			$t = $d[0];
			$r = $t[2].$t[3];
			?>
            <div class="col-sm-6">REPAIR #<?= $model->repair_id .'/'. $r;?></div>
            <div class="col-sm-6" align="right">
                <?
                $this->widget('bootstrap.widgets.TbButton', array(
                    'label' => 'Back',
                    'url' => Yii::app()->createAbsoluteUrl('crepair'),
                    'htmlOptions' => array('class' => 'menu_button')
                ));
                $this->widget('bootstrap.widgets.TbButton', array(
                    'label' => 'Print',
                    'url' => '',
                    'id' => 'print_me',
                    'htmlOptions' => array('class' => 'menu_button')
                ));

                ?>
            </div>
        </div>

        <div class="under_header_header col-sm-12">
            CUSTOMER INFORMATION
        </div>
        <?
        echo '<div class="col-sm-6"><table class="table table-striped table-bordered">';
        $a = 0; //counter for maiking another table
        foreach ($model->client->attributes as $client => $value) {
            if ($client == 'id' || $client == 'date' || $client == 'profession' || $client == 'payment_method' || $client == 'login' || $client == 'payment_method' || $client == 'password' || $client == 'active' || $client == 'role_id' || $client == 'comment') ;
            else {
                $a++;
                if ($a == 6) {
                    echo '</table></div><div class="col-sm-6"><table class="table table-striped table-bordered col-sm-6">';
                }
                echo '<tr><td>' . $model->client->getAttributeLabel($client) . '</td><td>' . $value . '</td></tr>';
            }
        }
        echo '</table></div>';
        ?>

        <?
        echo '<div class="under_header_header col-sm-12">';
        echo 'REPAIR INFORMATION';
        echo '</div>';
        echo '<div class="col-sm-6"><table class="table table-striped table-bordered">';
        $a = 0; //counter for making another table
        foreach ($model->attributes as $repair => $value) {
            if ($repair == 'repair_id' || $repair == 'send_method' || $repair == 'support_id') {
				if( $repair == 'support_id'){
				$a++;
				if ($a == 4) {
					echo '</table></div><div class="col-sm-6"><table class="table table-striped table-bordered col-sm-6">';
				}
				echo '<tr><td>' . $model->getAttributeLabel($repair) . '</td><td>' . ($value)?$value:'not set yet' .
					'</td></tr>';
				}
			}
            else {
                if ($repair == 'damage')
                    break;
                $a++;
                if ($a == 4) {
                    echo '</table></div><div class="col-sm-6"><table class="table table-striped table-bordered col-sm-6">';
                }
                echo '<tr><td>' . $model->getAttributeLabel($repair) . '</td><td>' . $value . '</td></tr>';
            }

        }
        echo '</table></div>';



		if(Repair::model()->findByPk(str_replace('#','',explode('/',$model->old_service_pswd)[0]))){
			$old_pwd_id = Repair::model()->findByPk(str_replace('#','',explode('/',$model->old_service_pswd)[0]));
			$history = Repair::model()->findAllByAttributes(array('repair_id'=>$old_pwd_id->repair_id));
			/** @var Repair $h */
			foreach($history as $h){
				$m = explode('-',$h->date)[0];
				echo '<br/><a href="'.Yii::app()->createAbsoluteUrl('repairs/details/'.$h->repair_id).'"> previos
				repair #'.$h->repair_id.'/'.$m[2].$m[3].'</a>';
			}
		}
        ?>

        <?
        //////////////////////////////////MENU FOR MODERATOR ///////////////////////////
        if(Yii::app()->user->checkAccess(User::ROLE_MODERATOR))
        {
            if($model->status == Repair::STATUS_SUCCESSFULLY_COMPLETED)
            {
                $disabled = '\'disabled\'';
            }
            else $disabled ='';
        echo '<div class="under_header_header col-sm-12 inner_comments">';
        echo 'REPAIR OPTIONS';
        echo '</div>';
        echo '<div class="col-sm-12 inner_comments">';
        echo '<div class="col-sm-8">';
        $form = $this->beginWidget('CActiveForm', array(
            'enableAjaxValidation' => false,
        ));
            echo '<div class="col-sm-5 input_label">';
            echo $form->labelEx($model, 'send_method');
            echo '</div><div class="col-sm-7 input_container">';
//            die(var_dump(Warehouse::model()->findAllByAttributes(array('status' => Warehouse::STATUS_EMPTY))));
            $a = CHtml::listData(SendMethod::model()->findAll(), 'method_id', 'name');
//            if(isset($model->warehouse->number)){$a[$model->warehouse->number] = $model->warehouse->number;}
//            die(var_dump($a));
            echo $form->dropDownList($model, 'send_method', $a, array('class' => 'form-control'));
            echo '</div>';


//            echo '<div class="col-sm-5 input_label">';
//            echo $form->labelEx($model, 'send_method');
//            echo '</div><div class="col-sm-7 input_container">';
//            echo $form->textField($model, 'send_method' ,array('value'=>$model->sendMethod->name, 'class' => 'form-control' ,'disabled'=>'disabled'));
//            echo '</div>';

        echo '<div class="col-sm-5 input_label">';
        echo $form->labelEx($model, 'support_id');
        echo '</div><div class="col-sm-7 input_container">';

			if(Yii::app()->user->checkAccess(User::ROLE_MODERATOR)){
				$asd = array('NULL'=>'not set yet');
				array_push($asd, CHtml::listData(User::model()->findAllByAttributes(array('role_id' =>
						User::ROLE_MODERATOR)),
					'id', 'login'));
				if(!$model->support_id) echo $form->dropDownList($model, 'support_id', array('Not set yet'),
					array('class' => 'form-control' ,'disabled'=>'disabled'));
				echo $form->dropDownList($model, 'support_id', $asd, array('class' => 'form-control' ,
					'disabled'=>$disabled));
			}else{
				if(!$model->support_id) echo $form->dropDownList($model, 'support_id', array('Not set yet'),
					array('class' => 'form-control' ,'disabled'=>'disabled'));

				else{
					echo $form->dropDownList($model, 'support_id', CHtml::listData(User::model()->findAllByAttributes(array('role_id' => User::ROLE_MODERATOR)), 'id', 'login'), array('class' => 'form-control' ,'disabled'=>$disabled));

				}
			}

        echo '</div>';

        echo '<div class="col-sm-5 input_label">';
        echo $form->labelEx($model, 'warehouse_id');
        echo '</div><div class="col-sm-7 input_container">';
//            die(var_dump(Warehouse::model()->findAllByAttributes(array('status' => Warehouse::STATUS_EMPTY))));
			$a = array('NULL'=>'Not set yet');

        array_push($a,CHtml::listData(Warehouse::model()->findAllByAttributes(array('status' =>
			Warehouse::STATUS_EMPTY)), 'number', 'number'));
            if(isset($model->warehouse->number)){$a[$model->warehouse->number] = $model->warehouse->number;}
//            die(var_dump($a));

            echo $form->dropDownList($model, 'warehouse_id', $a, array('class' => 'form-control' ,'disabled'=>$disabled));
        echo '</div>';

        echo '<div class="col-sm-5 input_label">';
        echo $form->labelEx($model, 'status');
        echo '</div><div class="col-sm-7 input_container">';
        echo $form->dropDownList($model, 'status', CHtml::listData(Colors::model()->findAll(), 'status', 'description'), array('class' => 'form-control' ,'disabled'=>$disabled));
        echo '</div>';

        echo '<div class="col-sm-5 input_label">';
        echo $form->labelEx($model, 'diagnostic');
        echo '</div><div class="col-sm-7 input_container">';
        echo $form->textField($model, 'diagnostic', array('class' => 'form-control' ,'disabled'=>$disabled));
        echo '</div>';
if($model->status > Repair::STATUS_WAITING_FOR_CUSTOMER_AGREEMENT)
{
    echo '<div class="col-sm-5 input_label">';
    echo $form->labelEx($model, 'price');
    echo '</div><div class="col-sm-7 input_container">';
    echo $form->textField($model, 'price', array('class' => 'form-control','disabled'=>'disabled'));
    echo '</div>';
}
            else{
        echo '<div class="col-sm-5 input_label">';
        echo $form->labelEx($model, 'price');
        echo '</div><div class="col-sm-7 input_container">';
        echo $form->textField($model, 'price', array('class' => 'form-control' ,'disabled'=>$disabled));
        echo '</div>';
            }
//            die(var_dump($model->status));

            echo '<div class="col-lg-offset-2 col-sm-8">';
            $this->widget(
                'bootstrap.widgets.TbButton',
                array('buttonType' => 'submit',
                    'label' => 'Save',
                    'htmlOptions' => array('class' => 'repair_button'))
            );
            echo '</div>';

        $this->endWidget();
        echo '</div>';
        echo '</div>';
        }

        else
        {

            echo '<div class="under_header_header col-sm-12 inner_comments">';
            echo 'REPAIR OPTIONS';
            echo '</div>';
            echo '<div class="col-sm-12 inner_comments">';
            echo '<div class="col-sm-8">';
            $form = $this->beginWidget('CActiveForm', array(
                'enableAjaxValidation' => false,
            ));
            echo '<div class="col-sm-5 input_label">';
            echo $form->labelEx($model, 'support_id');
            echo '</div><div class="col-sm-7 input_container">';
			$a = CHtml::listData(User::model()->findAllByAttributes(array('role_id' => User::ROLE_MODERATOR)), 'id',
				'login');
			array_push($a,array('NULL'=>'Not set yet'));
			if(Yii::app()->user->checkAccess(User::ROLE_MODERATOR)){
				if(!$model->support_id) echo $form->dropDownList($model, 'support_id', array('Not set yet'),
					array('class' => 'form-control' ,'disabled'=>'disabled'));
				echo $form->dropDownList($model, 'support_id',$a, array('class' => 'form-control'));
			}else{
				if(!$model->support_id) echo $form->dropDownList($model, 'support_id', array('Not set yet'),
					array('class' => 'form-control' ,'disabled'=>'disabled'));

				else{
					echo $form->dropDownList($model, 'support_id', $a, array('class' => 'form-control' ,
						'disabled'=>'disabled'));

				}
			}
            echo '</div>';

            echo '<div class="col-sm-5 input_label">';
            echo $form->labelEx($model, 'warehouse_id');
            echo '</div><div class="col-sm-7 input_container">';
			if($model->warehouse_id)
            echo $form->dropDownList($model, 'warehouse_id', CHtml::listData(Warehouse::model()->findAllByAttributes(array('status' => Warehouse::STATUS_EMPTY)), 'number', 'number'), array('class' => 'form-control','disabled'=>'disabled'));
            else
				echo $form->dropDownList($model, 'warehouse_id', array('Not set yet'), array('class' => 'form-control',
					'disabled'=>'disabled'));
			echo '</div>';

            echo '<div class="col-sm-5 input_label">';
            echo $form->labelEx($model, 'status');
            echo '</div><div class="col-sm-7 input_container">';
            echo $form->dropDownList($model, 'status', CHtml::listData(Colors::model()->findAll(), 'status', 'description'), array('class' => 'form-control','disabled'=>'disabled'));
            echo '</div>';

            echo '<div class="col-sm-5 input_label">';
            echo $form->labelEx($model, 'diagnostic');
            echo '</div><div class="col-sm-7 input_container">';
            echo $form->textField($model, 'diagnostic', array('class' => 'form-control','disabled'=>'disabled'));
            echo '</div>';
            echo '<div class="col-sm-5 input_label">';
            echo $form->labelEx($model, 'price');
            echo '</div><div class="col-sm-7 input_container">';
            echo $form->textField($model, 'price', array('class' => 'form-control','disabled'=>'disabled'));
            echo '</div>';

            if($model->status == Repair::STATUS_WAITING_FOR_CUSTOMER_AGREEMENT)
            {
                echo '<div class="col-sm-5 input_label">';
                echo '<label for="status">I\'m Agreed</label>';
                echo '</div><div class="col-sm-7 input_container">';
                echo $form->radioButtonList($model, 'status', array(Repair::STATUS_SENT_FOR_REPAIRS=>'I\'m Agreed with price', Repair::STATUS_CANCELED_BY_CUSTOMER=>'Cancel my order'));

                echo '</div>';
                echo '<div class="col-lg-offset-2 col-sm-8">';
                $this->widget(
                    'bootstrap.widgets.TbButton',
                    array('buttonType' => 'submit',
                        'label' => 'Save',
                        'htmlOptions' => array('class' => 'repair_button'))
                );
                echo '</div>';
            }

            if($model->status == Repair::STATUS_NEW_ORDER || $model->status == Repair::STATUS_WAITING_FOR_DIAGNOSTIC)
            {
                echo '<div class="col-sm-5 input_label">';
                echo '<label for="status">Cancel order</label>';
                echo '</div><div class="col-sm-7 input_container">';
                echo $form->checkBox($model, 'status', array('value'=>Repair::STATUS_CANCELED_BY_CUSTOMER,'class' => 'form-control'));
                echo '</div>';
                echo '<div class="col-lg-offset-2 col-sm-8">';
                $this->widget(
                    'bootstrap.widgets.TbButton',
                    array('buttonType' => 'submit',
                        'label' => 'Save',
                        'htmlOptions' => array('class' => 'repair_button'))
                );
                echo '</div>';
            }

            $this->endWidget();
            echo '</div>';
            echo '</div>';

        }
        ?>
        <div <? if(Yii::app()->user->checkAccess(User::ROLE_MODERATOR)){echo 'class="col-sm-6 user_comments"';}?>>
            <div class="under_header_header col-sm-12">
                CONTACT WITH CUSTOMER
                <?=CHtml::link('<span class="glyphicon glyphicon-plus"></span>', array('/repairs/addcomment/' . $model->repair_id), array('style' => 'color:#deb714', 'title' => 'Add new comment'));?>

            </div>
            <?
            //// it's comments do not touch them !!!!!!!!!!
            echo '<div class="col-sm-12">';
            foreach ($model->messages as $message) {
                if ($message->hidden == 0) {
                    echo '<div class="col-sm-12">';
                    echo '<div class="under_header_header">';
                    echo '<div class="col-sm-8">COMMENT #';
                    echo $message->message_id;

                    echo '</div>';
                    echo '<div class="col-sm-4" align="right">';
                    echo CHtml::link('<span class="glyphicon glyphicon-trash"></span>', array('/repairs/deletecomment/' . $message->message_id), array('style' => 'color:#deb714', 'title' => 'Delete'));
                    echo CHtml::link('<span class="glyphicon glyphicon-pencil"></span>', array('/repairs/editcomment/' . $message->message_id), array('style' => 'color:#deb714', 'title' => 'Edit'));
                    echo '</div></div>';
                    echo '<div class="col-sm-5 message no_padding">   From: ';
                    $login = User::model()->findByAttributes(array('id' => $message->from_id));
                    echo $login->login;

                    echo '</div>';
                    echo '<div class="col-sm-7 message no_padding" align="right">   Date: ' . $message->date;
                    echo '</div>';
                    echo '<div class="comment_body">';
                    echo $message->message;
                    echo '</div>';
                    echo '</div>';
                }
            }


            echo '</div></div>';


            if(Yii::app()->user->checkAccess(User::ROLE_MODERATOR)){
            echo '<div class="col-sm-6 inner_comments">';
            echo '    <div class="under_header_header col-sm-12">';
            echo 'INNER COMMUNICATION';
            echo CHtml::link('<span class="glyphicon glyphicon-plus"></span>', array('/repairs/AddHiddenComment/' . $model->repair_id), array('style' => 'color:#deb714', 'title' => 'Add new comment'));
            echo '</div>';

                //// it's comments do not touch them !!!!!!!!!!
                foreach ($model->messages as $message) {
                    if ($message->hidden == 1) {
                            echo '<div class="col-sm-12">';
                            echo '<div class="under_header_header">';
                            echo '<div class="col-sm-8">COMMENT #';
                            echo $message->message_id;
                            echo '</div>';
                            echo '<div class="col-sm-4" align="right">';
                            echo CHtml::link('<span class="glyphicon glyphicon-trash"></span>', array('/repairs/deletecomment/' . $message->message_id), array('style' => 'color:#deb714', 'title' => 'Delete'));
                            echo CHtml::link('<span class="glyphicon glyphicon-pencil"></span>', array('/repairs/editcomment/' . $message->message_id), array('style' => 'color:#deb714', 'title' => 'Edit'));
                            echo '</div></div>';
                            echo '<div class="col-sm-5 message no_padding">   From: ';
                            $login = User::model()->findByAttributes(array('id' => $message->from_id));
                            echo $login->login;

                            echo '</div>';
                            echo '<div class="col-sm-7 message no_padding" align="right">   Date: ' . $message->date;
                            echo '</div>';
                            echo '<div class="comment_body">';
                            echo $message->message;
                            echo '</div>';
                            echo '</div>';
                    }
                }
                }
                ?>


            </div>
        </div>
