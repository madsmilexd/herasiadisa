<div class="col-md-offset-2 col-sm-8 col-md-offset-2">
<div class="col-sm-12 reg_container">
<div class="reg_header col-sm-12">
    <div class="col-sm-6">PARTS ORDER #<?= $model->repair_id ?></div>
    <div class="col-sm-6" align="right">
        <?
        $this->widget('bootstrap.widgets.TbButton', array(
            'label' => 'Back',
            'url' => Yii::app()->createAbsoluteUrl('crepair'),
            'htmlOptions' => array('class' => 'menu_button')
        ));
        $this->widget('bootstrap.widgets.TbButton', array(
            'label' => 'Print',
            'url' => '',
            'id' => 'print_me',
            'htmlOptions' => array('class' => 'menu_button')
        ));

        ?>
    </div>
</div>

<div class="under_header_header col-sm-12">
    CUSTOMER INFORMATION
</div>
<?
echo '<div class="col-sm-6"><table class="table table-striped table-bordered">';
$a = 0; //counter for maiking another table
foreach ($model->client->attributes as $client => $value) {
    if ($client == 'id' || $client == 'date' || $client == 'profession' || $client == 'payment_method' || $client == 'login' || $client == 'payment_method' || $client == 'password' || $client == 'active' || $client == 'role_id' || $client == 'comment') ;
    else {
        $a++;
        if ($a == 6) {
            echo '</table></div><div class="col-sm-6"><table class="table table-striped table-bordered col-sm-6">';
        }
        echo '<tr><td>' . $model->client->getAttributeLabel($client) . '</td><td>' . $value . '</td></tr>';
    }
}
echo '</table></div>';
?>

<?
echo '<div class="under_header_header col-sm-12">';
echo 'SPARE PART INFORMATION';
echo '</div>';

echo '<div class="col-sm-6"><table class="table table-striped table-bordered col-sm-6">';
echo '<tr><td>Type</td><td>'.$model->damage.'</td>';
echo '<tr><td>Order Details</td><td>'.$model->diagnostic.'</td>';
echo '<tr><td>Image</td><td><img width="100%" src="'.Attachment::model()->getImageByRepairsId($model->repair_id)
	.'"/></td>';
echo '</table></div>';

?>

<?
//////////////////////////////////MENU FOR MODERATOR ///////////////////////////
if(Yii::app()->user->checkAccess(User::ROLE_MODERATOR))
{
    if($model->status == Repair::STATUS_SUCCESSFULLY_COMPLETED)
    {
        $disabled = '\'disabled\'';
    }
    else $disabled ='';
    echo '<div class="under_header_header col-sm-12">';
    echo 'REPAIR OPTIONS';
    echo '</div>';
    echo '<div class="col-sm-12">';
    echo '<div class="col-sm-8">';
    $form = $this->beginWidget('CActiveForm', array(
        'enableAjaxValidation' => false,
    ));

    echo '<div class="col-sm-5 input_label">';
    echo $form->labelEx($model, 'status');
    echo '</div><div class="col-sm-7 input_container">';
    echo $form->dropDownList($model, 'status', array('0'=>'New order','2'=>'Waiting for customer agreement','4'=>'Waiting for spair parts','5'=>'Spare Parts derived'), array('class' => 'form-control' ,'disabled'=>$disabled));
//    echo $form->dropDownList($model, 'status', CHtml::listData(Colors::model()->findAll(), 'status', 'description'), array('class' => 'form-control' ,'disabled'=>$disabled));
    echo '</div>';

    if($model->status > Repair::STATUS_WAITING_FOR_CUSTOMER_AGREEMENT)
    {
        echo '<div class="col-sm-5 input_label">';
        echo $form->labelEx($model, 'price');
        echo '</div><div class="col-sm-7 input_container">';
        echo $form->textField($model, 'price', array('class' => 'form-control','disabled'=>'disabled'));
        echo '</div>';
    }
    else{
        echo '<div class="col-sm-5 input_label">';
        echo $form->labelEx($model, 'price');
        echo '</div><div class="col-sm-7 input_container">';
        echo $form->textField($model, 'price', array('class' => 'form-control' ,'disabled'=>$disabled));
        echo '</div>';
    }
//            die(var_dump($model->status));

        echo '<div class="col-lg-offset-2 col-sm-8">';
        $this->widget(
            'bootstrap.widgets.TbButton',
            array('buttonType' => 'submit',
                'label' => 'Save',
                'htmlOptions' => array('class' => 'repair_button'))
        );
        echo '</div>';

    $this->endWidget();
    echo '</div>';
    echo '</div>';
}

else
{

    echo '<div class="under_header_header col-sm-12">';
    echo 'REPAIR OPTIONS';
    echo '</div>';
    echo '<div class="col-sm-12">';
    echo '<div class="col-sm-8">';
    $form = $this->beginWidget('CActiveForm', array(
        'enableAjaxValidation' => false,
    ));

    echo '<div class="col-sm-5 input_label">';
    echo $form->labelEx($model, 'status');
    echo '</div><div class="col-sm-7 input_container">';
    echo $form->dropDownList($model, 'status', CHtml::listData(Colors::model()->findAll(), 'status', 'description'), array('class' => 'form-control','disabled'=>'disabled'));
    echo '</div>';
    echo '<div class="col-sm-5 input_label">';
    echo $form->labelEx($model, 'price');
    echo '</div><div class="col-sm-7 input_container">';
    echo $form->textField($model, 'price', array('class' => 'form-control','disabled'=>'disabled'));
    echo '</div>';

    if($model->status == Repair::STATUS_WAITING_FOR_CUSTOMER_AGREEMENT)
    {
        echo '<div class="col-sm-5 input_label">';
        echo '<label for="status">I\'m Agreed</label>';
        echo '</div><div class="col-sm-7 input_container">';
        echo $form->radioButtonList($model, 'status', array(Repair::STATUS_WAITING_FOR_SPARE_PARTS=>'I\'m Agreed with price',
			Repair::STATUS_CANCELED_BY_CUSTOMER=>'Cancel my order'));

        echo '</div>';
        echo '<div class="col-lg-offset-2 col-sm-8">';
        $this->widget(
            'bootstrap.widgets.TbButton',
            array('buttonType' => 'submit',
                'label' => 'Save',
                'htmlOptions' => array('class' => 'repair_button'))
        );
        echo '</div>';
    }

    if($model->status == Repair::STATUS_NEW_ORDER || $model->status == Repair::STATUS_WAITING_FOR_DIAGNOSTIC)
    {
        echo '<div class="col-sm-5 input_label">';
        echo '<label for="status">Cancel order</label>';
        echo '</div><div class="col-sm-7 input_container">';
        echo $form->checkBox($model, 'status', array('value'=>Repair::STATUS_CANCELED_BY_CUSTOMER,'class' => 'form-control'));
        echo '</div>';
        echo '<div class="col-lg-offset-2 col-sm-8">';
        $this->widget(
            'bootstrap.widgets.TbButton',
            array('buttonType' => 'submit',
                'label' => 'Save',
                'htmlOptions' => array('class' => 'repair_button'))
        );
        echo '</div>';
    }

    $this->endWidget();
    echo '</div>';
    echo '</div>';

}
?>
<div <? if(Yii::app()->user->checkAccess(User::ROLE_MODERATOR)){echo 'class="col-sm-6"';}?>>
    <div class="under_header_header col-sm-12">
        CONTACT WITH CUSTOMER
        <?=CHtml::link('<span class="glyphicon glyphicon-plus"></span>', array('/repairs/addcomment/' . $model->repair_id), array('style' => 'color:#deb714', 'title' => 'Add new comment'));?>

    </div>
    <?
    //// it's comments do not touch them !!!!!!!!!!
    echo '<div class="col-sm-12">';
    foreach ($model->messages as $message) {
        if ($message->hidden == 0) {
            echo '<div class="col-sm-12">';
            echo '<div class="under_header_header">';
            echo '<div class="col-sm-8">COMMENT #';
            echo $message->message_id;

            echo '</div>';
            echo '<div class="col-sm-4" align="right">';
            echo CHtml::link('<span class="glyphicon glyphicon-trash"></span>', array('/repairs/deletecomment/' . $message->message_id), array('style' => 'color:#deb714', 'title' => 'Delete'));
            echo CHtml::link('<span class="glyphicon glyphicon-pencil"></span>', array('/repairs/editcomment/' . $message->message_id), array('style' => 'color:#deb714', 'title' => 'Edit'));
            echo '</div></div>';
            echo '<div class="col-sm-5 message no_padding">   From: ';
            $login = User::model()->findByAttributes(array('id' => $message->from_id));
            echo $login->login;

            echo '</div>';
            echo '<div class="col-sm-7 message no_padding" align="right">   Date: ' . $message->date;
            echo '</div>';
            echo '<div class="comment_body">';
            echo $message->message;
            echo '</div>';
            echo '</div>';
        }
    }


    echo '</div></div>';


    if(Yii::app()->user->checkAccess(User::ROLE_MODERATOR)){
        echo '<div class="col-sm-6">';
        echo '    <div class="under_header_header col-sm-12">';
        echo 'INNER COMMUNICATION';
        echo CHtml::link('<span class="glyphicon glyphicon-plus"></span>', array('/repairs/addcomment/' . $model->repair_id), array('style' => 'color:#deb714', 'title' => 'Add new comment'));
        echo '</div>';

        //// it's comments do not touch them !!!!!!!!!!
        foreach ($model->messages as $message) {
            if ($message->hidden == 1) {
                echo '<div class="col-sm-12">';
                echo '<div class="under_header_header">';
                echo '<div class="col-sm-8">COMMENT #';
                echo $message->message_id;
                echo '</div>';
                echo '<div class="col-sm-4" align="right">';
                echo CHtml::link('<span class="glyphicon glyphicon-trash"></span>', array('/repairs/deletecomment/' . $message->message_id), array('style' => 'color:#deb714', 'title' => 'Delete'));
                echo CHtml::link('<span class="glyphicon glyphicon-pencil"></span>', array('/repairs/editcomment/' . $message->message_id), array('style' => 'color:#deb714', 'title' => 'Edit'));
                echo '</div></div>';
                echo '<div class="col-sm-5 message no_padding">   From: ';
                $login = User::model()->findByAttributes(array('id' => $message->from_id));
                echo $login->login;

                echo '</div>';
                echo '<div class="col-sm-7 message no_padding" align="right">   Date: ' . $message->date;
                echo '</div>';
                echo '<div class="comment_body">';
                echo $message->message;
                echo '</div>';
                echo '</div>';
            }
        }
    }
    ?>


</div>
</div>
