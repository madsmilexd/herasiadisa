<?php
/* @var $this RepairsController */
/* @var $model Repair */
/* @var $form CActiveForm */
?>

<div class="form">

    <?php $form = $this->beginWidget('CActiveForm', array(
        'id' => 'repair-new-form',
        // Please note: When you enable ajax validation, make sure the corresponding
        // controller action is handling ajax validation correctly.
        // See class documentation of CActiveForm for details on this,
        // you need to use the performAjaxValidation()-method described there.
        'enableAjaxValidation' => false,
    ));

    ?>
    <div class="col-md-offset-2 col-sm-8 col-md-offset-2">
        <div class="col-lg-12 reg_container">
            <div class="reg_header col-sm-12">
                <div class="col-lg-6">ADD NEW SPARE PARTS ORDER</div>
                <div class="col-lg-6" align="right">
                    <?
                    $this->widget('bootstrap.widgets.TbButton', array(
                        'label' => 'Print',
                        'url' => '',
                        'id'=>'print_me',
                        'htmlOptions' => array('class' => 'menu_button')
                    ));

                    ?>
                </div>
            </div>
            <div class="col-lg-6" style="padding: 0px;">

                <div class="col-lg-12" style="padding: 0px 0px 0px 0px !important;">
                    <div id="dropZone" style="padding: 0px 0px 0px 0px !important;height: 200px;">
                        To upload, drag the file here.
                    </div>
                </div>

                <? if(Yii::app()->user->checkAccess(User::ROLE_MODERATOR))
                {?>
                    <div class="col-lg-12">
                        <div class="col-lg-5 input_label">
                            <?= $form->labelEx($model, 'client_id') ?>
                        </div>
                        <div class="col-lg-7 input_container">
                            <?                $a = CHtml::listData(User::model()->findAllByAttributes(array('role_id' => User::ROLE_CUSTOMER)), 'id', 'login'); ?>
                            <?= $form->dropDownList($model, 'client_id', $a, array('class' => 'form-control')) ?>
                        </div>
                    </div>
                <? } ?>

                <div class="col-lg-12">
                    <div class="col-lg-5 input_label">
                    <?= '<label for="status">Type</label>'; ?></div>
                <div class="col-lg-7 input_container">
                        <?php echo $form->textField($model, 'damage', array('class' => 'form-control', 'rows'=>'3')); ?>
                    </div>
                </div>



                <div class="col-lg-12">
                    <div class="col-lg-5 input_label">
                        <?= '<label for="status">Order Details</label>'; ?></div>
                    <div class="col-lg-7 input_container">
                        <?= $form->textArea($model, 'diagnostic', array('class' => 'form-control')); ?>
                    </div>
                </div>

				<div class="col-lg-12 hidden">
                    <div class="col-lg-5 input_label">
                        <?= '<label for="status">attachment</label>'; ?></div>
                    <div class="col-lg-7 input_container">
                        <?= $form->textArea($model, 'attachment', array('class' => 'form-control attachment-img')); ?>
                    </div>
                </div>

            </div>
			<div class="attachd">

			</div>


                <div class="col-lg-offset-2 col-sm-8 input_container">
                    <? $this->widget(
                        'bootstrap.widgets.TbButton',
                        array('buttonType' => 'submit',
                            'label' => 'Save',
                            'htmlOptions' => array('class' => 'save_button'))
                    );
                    ?>
                </div>
            <div class="col-lg-12 error-block" align="center">

				<?
				if($model->hasErrors()){
					foreach($model->errors as $r){
						echo $r[0].'<br/>';
					}
				}
				?>
				<?// $form->errorSummary($model); ?>
            </div>
            </div>



            <?php $this->endWidget(); ?>

        </div>
