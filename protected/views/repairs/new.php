<?php
/* @var $this RepairController */
/* @var $model Repair */
/* @var $form CActiveForm */
?>

<div class="form">

    <?php $form = $this->beginWidget('CActiveForm', array(
        'id' => 'repair-new-form',
        // Please note: When you enable ajax validation, make sure the corresponding
        // controller action is handling ajax validation correctly.
        // See class documentation of CActiveForm for details on this,
        // you need to use the performAjaxValidation()-method described there.
        'enableAjaxValidation' => false,
    ));

    ?>
    <div class="col-md-offset-2 col-sm-8 col-md-offset-2">
        <div class="col-lg-12 reg_container">
            <div class="reg_header col-sm-12">
                <div class="col-lg-6">ADD NEW REPAIR</div>
                <div class="col-lg-6" align="right">
                    <?
					$this->widget('bootstrap.widgets.TbButton', array(
						'label' => 'Print',
						'url' => '',
						'id'=>'print_me',
						'htmlOptions' => array('class' => 'menu_button')
					));

                    ?>
                </div>
            </div>
    <div class="col-lg-6" style="padding: 0px;">
        <? if(Yii::app()->user->checkAccess(User::ROLE_MODERATOR))
        {?>
        <div class="col-lg-12">
            <div class="col-lg-5 input_label">
                <?= $form->labelEx($model, 'client_id') ?>
            </div>
            <div class="col-lg-7 input_container">
<?                $a = CHtml::listData(User::model()->findAllByAttributes(array('role_id' => User::ROLE_CUSTOMER)), 'id', 'login'); ?>
                <?= $form->dropDownList($model, 'client_id', $a, array('class' => 'form-control')) ?>
            </div>
        </div>
       <? } ?>
        <div class="col-lg-12">
            <div class="col-lg-5 input_label">
                <?= $form->labelEx($model, 'brand_id') ?>
            </div>
            <div class="col-lg-7 input_container">
                <?= $form->dropDownList($model, 'brand_id', Brand::model()->getBrands(), array('class' => 'form-control')) ?>
            </div>
        </div>
        <div class="col-lg-12">
            <div class="col-lg-5 input_label">
                <?php echo $form->labelEx($model, 'model'); ?></div>
            <div class="col-lg-7 input_container">
                <?php echo $form->textField($model, 'model', array('class' => 'form-control')); ?>
            </div>
        </div>

        <div class="col-lg-12">
            <div class="col-lg-5 input_label">
                <?php echo $form->labelEx($model, 'accessories'); ?></div>
            <div class="col-lg-7 input_container">
				<sub><p>To check more than one</p><p> press ctrl+ left click</p></sub>
				<select class="form-control accessories_list" multiple>
					<option value="Battery">Battery</option>
					<option value="Power_supply">Power supply</option>
					<option value="Bag">Bag</option>
					<option value="Other">Other</option>
				</select>
				<input type="text" class="other_accessories" hidden="hidden" />
            </div>
			<?= $form->textField($model, 'accessories',
				array('class' => 'form-control accessories','style'=>'display:none','multiple'=>true));?>
        </div>


        <div class="col-lg-12">
            <div class="col-lg-5 input_label">
                <?php echo $form->labelEx($model, 'send_method'); ?></div>
            <div class="col-lg-7 input_container">
                <?= $form->dropDownList($model, 'send_method', SendMethod::model()->getMethods(), array('class' => 'form-control')); ?>
            </div>
        </div>

        <div class="col-lg-12">
            <div class="col-lg-5 input_label">
                <?php echo $form->labelEx($model, 'damage'); ?></div>
            <div class="col-lg-7 input_container">
                <?php echo $form->textArea($model, 'damage', array('class' => 'form-control', 'rows'=>'3')); ?>
            </div>
        </div>



    </div>
    <div class="col-lg-6" style="padding: 0px;">




        <div class="col-lg-12">
            <div class="col-lg-5 input_label">
                <?php echo $form->labelEx($model, 'old_service_pswd'); ?>
            </div>
            <div class="col-lg-7 input_container">
                <?php echo $form->textField($model, 'old_service_pswd', array('class' => 'form-control')); ?>
            </div>
        </div>

        <div class="col-lg-12">
            <div class="col-lg-5 input_label">
                <?php echo $form->labelEx($model, 'tracking_number'); ?>
            </div>
            <div class="col-lg-7 input_container">
                <?php echo $form->textField($model, 'tracking_number', array('class' => 'form-control')); ?>
            </div>
        </div>

        <div class="col-lg-12">
            <div class="col-lg-5 input_label">
                <?php echo $form->labelEx($model, 'shiping_number'); ?>
            </div>
            <div class="col-lg-7 input_container">
                <?php echo $form->textField($model, 'shiping_number', array('class' => 'form-control')); ?>
            </div>
        </div>




        <div class="col-lg-offset-2 col-sm-8 input_container">
            <? $this->widget(
                'bootstrap.widgets.TbButton',
                array('buttonType' => 'submit',
                    'label' => 'Save',
                    'htmlOptions' => array('class' => 'save_button'))
            );
            ?>
        </div>
    </div>
    <div class="col-lg-12 error-block" align="center">
        <?
		if($model->hasErrors()){
	foreach($model->errors as $e){
		echo "<div style='color:red;'>{$e}</div>";
	}
		}
		?>
    </div>


    <?php $this->endWidget(); ?>

</div>
