<?php
/* @var $this ColorsController */
/* @var $model Colors */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'colors-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>


	<?
	if($model->hasErrors()){
		foreach($model->errors as $r){
			echo $r[0].'<br/>';
		}
	}
	?>
    <div class="col-lg-offset-2 col-sm-8">

        <div class="col-sm-4 input_label" style="margin-top: 10px">
		<?php echo $form->labelEx($model,'color'); ?>
        </div>
        <div class="col-sm-8" style="margin-top: 10px">
        <?php echo $form->textField($model,'color',array('class'=>'form-control','maxlength'=>6)); ?>
	    </div>

        <div class="col-sm-4 input_label" style="margin-top: 10px">
		<?php echo $form->labelEx($model,'status'); ?>
        </div>
        <div class="col-sm-8" style="margin-top: 10px">
        <?php echo $form->textField($model,'status',array('class'=>'form-control')); ?>
	    </div>

        <div class="col-sm-4 input_label" style="margin-top: 10px">
		<?php echo $form->labelEx($model,'description'); ?>
        </div>
        <div class="col-sm-8" style="margin-top: 10px">
        <?php echo $form->textField($model,'description',array('class'=>'form-control')); ?>
    	</div>

<div class="col-lg-offset-3">
    <?
    $this->widget(
        'bootstrap.widgets.TbButton',
        array('buttonType' => 'submit',
            'label' => 'Save',
            'htmlOptions' => array('class' => 'repair_button'))
    );
    ?>
</div>

<?php $this->endWidget(); ?>

</div><!-- form -->