<?php
$form = $this->beginWidget('CActiveForm', array(
    'id' => 'user-reg-form',
    'enableAjaxValidation' => false,
    'htmlOptions' => array('class' => 'form-horizontal'),
));
?>
<div class="col-md-offset-2 col-sm-8 col-md-offset-2">
    <div class="col-lg-12 reg_container">
        <div class="reg_header col-sm-12">
            <div class="col-lg-6"><? if(Yii::app()->controller->action->id !== 'edit'){echo 'NEW COMMENT TO REPAIR';} else {echo 'EDIT COMMENT';}?></div>
            <div class="col-lg-6" align="right">
                <?
                $this->widget('bootstrap.widgets.TbButton', array(
                    'label' => 'Back',
                    'url' => Yii::app()->createAbsoluteUrl('/repairs/details/'.$id),
                    'htmlOptions' => array('class' => 'menu_button')
                ));
                $this->widget('bootstrap.widgets.TbButton', array(
                    'label' => 'Save',
                    'buttonType' => 'submit',
                    'htmlOptions' => array('class' => 'menu_button')
                ));
                ?>
            </div>
        </div>
        <div class="col-lg-5 input_label">
            <?=$form->labelEx($model, 'message')?>
        </div>

        <div class="col-lg-7 input_container">
            <?=$form->textArea($model, 'message',array('class'=>'form-control', 'rows'=>6)) ?>
        </div>


<?$this->endWidget();?>