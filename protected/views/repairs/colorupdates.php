<div class="col-md-offset-2 col-sm-8 col-md-offset-2">
    <div class="col-sm-12 reg_container">
        <div class="reg_header col-sm-12">
            <div class="col-sm-6">UPDATE COLOR <?=$model->id?></div>
            <div class="col-sm-6" align="right">
                <?
                $this->widget('bootstrap.widgets.TbButton', array(
                    'label' => 'Back',
                    'url' => Yii::app()->createAbsoluteUrl(Yii::app()->controller->id.'/palette'),
                    'htmlOptions' => array('class' => 'menu_button')
                ));
                ?>
            </div>
        </div>
        <?php
/* @var $this ColorsController */
/* @var $model Colors */

$this->breadcrumbs=array(
	'Colors'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List Colors', 'url'=>array('index')),
	array('label'=>'Create Colors', 'url'=>array('create')),
	array('label'=>'View Colors', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage Colors', 'url'=>array('admin')),
);
?>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>
        </div></div>