
<div class="col-md-offset-2 col-lg-8 col-md-offset-2">
    <div class="col-lg-12 reg_container">
        <div class="reg_header col-sm-12">
            <div class="col-lg-6">USER MANAGEMENT</div>
            <div class="col-lg-6" align="right">
                <?
                $this->widget('bootstrap.widgets.TbButton', array(
                    'label'=>'Back',
                    'url'=>Yii::app()->createAbsoluteUrl('/'),
                    'htmlOptions'=>array('class'=>'menu_button')
                ));
                $this->widget(
                    'bootstrap.widgets.TbButton',
                    array('buttonType' => 'url',
                        'label' => 'Add new user',
                        'url'=> Yii::app()->createAbsoluteUrl('user/add'),
                        'htmlOptions'=>array('class'=>'menu_button'))
                );
                ?>
                </div>
</div>
<table class="table table-striped table-bordered">
    <?

    $model2 = new User();
    $model2 = $model2->attributeLabels();
    echo '<tr align="center">';
    echo '<td>'.$model2[id].'</td>';
    echo '<td>Customer Name</td>';
    echo '<td>'.$model2[login].'</td>';
    echo '<td>'.$model2[email].'</td>';
    echo '<td>'.$model2[active].'</td>';
    echo '<td></td>';
    echo '<td></td>';
    echo '<tr>';

    foreach($model as $user)
    {
        echo '<tr>';
        echo '<td><a href="/index.php/user/profile/'.$user->id.'">'.$user->id.'</a></td>';
        echo '<td><a href="/index.php/user/profile/'.$user->id.'">'.$user->first_name.' '.$user->last_name.'</a></td>';
        echo '<td><a href="/index.php/user/profile/'.$user->id.'">'.$user->login.'</a></td>';
        echo '<td><a href="/index.php/user/profile/'.$user->id.'">'.$user->email.'</a></td>';
        echo '<td><a href="/index.php/user/profile/'.$user->id.'">';
        if($user->active == 0)
        echo 'Inactive';
        else echo 'Active';
        echo'</a></td>';
        echo '<td class="trash_button">';

        echo CHtml::link('<span class="glyphicon glyphicon-trash"></span>', array('/user/delete/' . $user->id), array('style' => 'color:#deb714', 'title'=>'Delete'));
        echo '</td>';
        echo '<td class="trash_button">';

        echo CHtml::link('<span class="glyphicon glyphicon-pencil"></span>', array('/user/edit/' . $user->id), array('style' => 'color:#deb714', 'title'=>'Edit'));

        echo '</td>';
        echo '</tr>';
    }
    ?>
</table>
</div>
</div>
