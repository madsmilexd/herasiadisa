<?php
$form = $this->beginWidget('CActiveForm', array(
    'id' => 'user-reg-form',
    'enableAjaxValidation' => false,
    'htmlOptions' => array('class' => 'form-horizontal'),
));
if (Yii::app()->controller->action->id == "edit") {
    if ($model->id == Yii::app()->user->id || Yii::app()->user->checkAccess(User::ROLE_MODERATOR) == true) {
        $disabled = false;
    } else $disabled = true;
} else $disabled = true;

?>
    <div class="col-md-offset-2 col-sm-8 col-md-offset-2">
    <div class="col-lg-12 reg_container">
        <div class="reg_header col-sm-12">
            <div class="col-lg-6">PROFILE</div>
            <div class="col-lg-6">

            </div>
        </div>
    <div class="col-lg-6" style="padding: 0px;">
        <?php echo $form->errorSummary($model); ?>
        <div class="col-lg-5 input_label">
            <?= $form->labelEx($model, 'first_name') ?>
        </div>
        <div class="col-lg-7 input_container">
            <?= $form->textField($model, 'first_name', array('class' => 'form-control', 'disabled' => $disabled)) ?>
        </div>
        <div class="col-lg-5 input_label">
            <?= $form->labelEx($model, 'last_name') ?>
        </div>
        <div class="col-lg-7 input_container">
            <?= $form->TextField($model, 'last_name', array('class' => 'form-control', 'disabled' => $disabled)) ?>
        </div>
        <div class="col-lg-5 input_label">
            <?= $form->labelEx($model, 'address') ?>
        </div>
        <div class="col-lg-7 input_container">
            <?= $form->textField($model, 'address', array('class' => 'form-control', 'disabled' => $disabled)) ?>
        </div>
        <div class="col-lg-5 input_label">
            <?= $form->labelEx($model, 'city') ?>
        </div>
        <div class="col-lg-7 input_container">
            <?= $form->textField($model, 'city', array('class' => 'form-control', 'disabled' => $disabled)) ?>
        </div>
        <div class="col-lg-5 input_label">
            <?= $form->labelEx($model, 'phone_numb') ?>
        </div>
        <div class="col-lg-7 input_container">
            <?= $form->textField($model, 'phone_numb', array('class' => 'form-control', 'disabled' => $disabled)) ?>
        </div>
        <div class="col-lg-5 input_label">
            <?= $form->labelEx($model, 'cell_phone') ?>
        </div>
        <div class="col-lg-7 input_container">
            <?= $form->textField($model, 'cell_phone', array('class' => 'form-control', 'disabled' => $disabled)) ?>
        </div>
        <div class="col-lg-5 input_label">
            <?= $form->labelEx($model, 'company_name') ?>
        </div>
        <div class="col-lg-7 input_container">
            <?= $form->textField($model, 'company_name', array('class' => 'form-control', 'disabled' => $disabled)) ?>
        </div>
        <div class="col-lg-5 input_label">
            <?= $form->labelEx($model, 'profession') ?>
        </div>
        <div class="col-lg-7 input_container">
            <?= $form->textField($model, 'profession', array('class' => 'form-control', 'disabled' => $disabled)) ?>
        </div>
        <div class="col-lg-5 input_label">
            <?= $form->labelEx($model, 'tax_agency') ?>
        </div>
        <div class="col-lg-7 input_container">
            <?= $form->TextField($model, 'tax_agency', array('class' => 'form-control', 'disabled' => $disabled)) ?>
        </div>
        <div class="col-lg-5 input_label">
            <?= $form->labelEx($model, 'vat_number') ?>
        </div>
        <div class="col-lg-7 input_container">
            <?= $form->TextField($model, 'vat_number', array('class' => 'form-control', 'disabled' => $disabled)) ?>
        </div>
        <div class="col-lg-5 input_label">
            <?= $form->labelEx($model, 'payment_method') ?>
        </div>
        <div class="col-lg-7 input_container">
            <?= $form->textField($model, 'payment_method', array('class' => 'form-control', 'disabled' => $disabled)) ?>
        </div>
    </div>

    <div class="col-lg-6" style="padding: 0px;">
        <div class="col-lg-5 input_label">
            <?= $form->labelEx($model, 'email') ?>
        </div>
        <div class="col-lg-7 input_container">
            <?= $form->textField($model, 'email', array('class' => 'form-control', 'disabled' => $disabled)) ?>
        </div>
        <div class="col-lg-5 input_label">
            <?= $form->labelEx($model, 'login') ?>
        </div>
        <div class="col-lg-7 input_container">
            <?= $form->textField($model, 'login', array('class' => 'form-control', 'disabled' => $disabled)) ?>
        </div>
        <div class="col-lg-5 input_label">
            <?= $form->labelEx($model, 'password') ?>
        </div>
        <div class="col-lg-7 input_container">
            <?= $form->passwordField($model, 'password', array('class' => 'form-control', 'disabled' => $disabled)) ?>
        </div>
        <div class="col-lg-12 input_label" align="center">
            <?= $form->labelEx($model, 'comment') ?>
        </div>
        <div class="col-lg-12 input_container">
            <?= $form->textarea($model, 'comment', array('class' => 'form-control', 'rows' => '14', 'disabled' => $disabled)) ?>
        </div>
        <?
        if (Yii::app()->controller->action->id == "profile")
        {
            echo '<div class="col-lg-6 input_container">';
            $this->widget(
                'bootstrap.widgets.TbButton',
                array(
                    'label' => 'Edit Details',
                    'url' =>  Yii::app()->createAbsoluteUrl('/user/edit/' . $model->id),
                    'htmlOptions' => array('class' => 'save_button'))
            );
            echo '</div>';
        }
        if (yii::app()->controller->action->id == "edit" && ($model->id == Yii::app()->user->id || Yii::app()->user->checkAccess(User::ROLE_MODERATOR) == true)) {

            echo '<div class="col-lg-6 input_container">';

            $this->widget(
                'bootstrap.widgets.TbButton',
                array(
                    'label' => 'Back',
                    'url' =>  Yii::app()->createAbsoluteUrl('user/profile/'),
                    'htmlOptions' => array('class' => 'save_button'))
            );
            echo '</div>';
            echo '<div class="col-lg-6 input_container">';
            $this->widget(
                'bootstrap.widgets.TbButton',
                array(
                    'label' => 'Save',
                    'buttonType' => 'submit',
                    'htmlOptions' => array('class' => 'save_button'))
            );
        }
        ?>


    </div>

    </div>
    </div>


<? $this->endWidget(); ?>