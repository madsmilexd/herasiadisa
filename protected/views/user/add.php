<?php
$form = $this->beginWidget('CActiveForm', array(
    'id' => 'user-reg-form',
    'enableAjaxValidation' => false,
    'htmlOptions' => array('class' => 'form-horizontal'),
));
?>

    <div class="col-md-offset-2 col-sm-8 col-md-offset-2">
        <div class="col-lg-12 reg_container">
            <div class="reg_header col-sm-12">
                <div class="col-lg-6">NEW CUSTOMER</div>
                <div class="col-lg-6" align="right">
                    <?
                    $this->widget('bootstrap.widgets.TbButton', array(
                        'label'=>'Back',
                        'url'=>Yii::app()->createAbsoluteUrl('/'),
                        'htmlOptions'=>array('class'=>'menu_button')
                    ));
                    ?>
                    <?
                    $this->widget(
                        'bootstrap.widgets.TbButton',
                        array('buttonType' => 'submit',
                            'label' => 'Save',
                            'htmlOptions'=>array('class'=>'menu_button'))
                    );

                    ?>
                </div>
            </div>
            <div class="col-lg-6" style="padding: 0px;">

                <div class="col-lg-5 input_label">
                    <?=$form->labelEx($model, 'first_name')?>
                </div>
                <div class="col-lg-7 input_container">
                    <?=$form->textField($model, 'first_name',array('class'=>'form-control'))?>
                </div>
                <div class="col-lg-5 input_label">
                    <?=$form->labelEx($model, 'last_name')?>
                </div>
                <div class="col-lg-7 input_container">
                    <?=$form->TextField($model, 'last_name',array('class'=>'form-control')) ?>
                </div>
                <div class="col-lg-5 input_label">
                    <?=$form->labelEx($model, 'address')?>
                </div>
                <div class="col-lg-7 input_container">
                    <?=$form->textField($model, 'address',array('class'=>'form-control'))?>
                </div>
                <div class="col-lg-5 input_label">
                    <?=$form->labelEx($model, 'city')?>
                </div>
                <div class="col-lg-7 input_container">
                    <?=$form->textField($model, 'city',array('class'=>'form-control')) ?>
                </div>
                <div class="col-lg-5 input_label">
                    <?=$form->labelEx($model, 'phone_numb')?>
                </div>
                <div class="col-lg-7 input_container">
                    <?=$form->textField($model, 'phone_numb',array('class'=>'form-control'))?>
                </div>
                <div class="col-lg-5 input_label">
                    <?=$form->labelEx($model, 'cell_phone')?>
                </div>
                <div class="col-lg-7 input_container">
                    <?=$form->textField($model, 'cell_phone',array('class'=>'form-control')) ?>
                </div>
                <div class="col-lg-5 input_label">
                    <?=$form->labelEx($model, 'company_name')?>
                </div>
                <div class="col-lg-7 input_container">
                    <?=$form->textField($model, 'company_name',array('class'=>'form-control'))?>
                </div>
                <div class="col-lg-5 input_label">
                    <?=$form->labelEx($model, 'profession')?>
                </div>
                <div class="col-lg-7 input_container">
                    <?=$form->textField($model, 'profession',array('class'=>'form-control'))?>
                </div>
                <div class="col-lg-5 input_label">
                    <?=$form->labelEx($model, 'tax_agency')?>
                </div>
                <div class="col-lg-7 input_container">
                    <?=$form->TextField($model, 'tax_agency',array('class'=>'form-control')) ?>
                </div>
                <div class="col-lg-5 input_label">
                    <?=$form->labelEx($model, 'vat_number')?>
                </div>
                <div class="col-lg-7 input_container">
                    <?=$form->TextField($model, 'vat_number',array('class'=>'form-control')) ?>
                </div>
                <div class="col-lg-5 input_label">
                    <?=$form->labelEx($model, 'payment_method')?>
                </div>
                <div class="col-lg-7 input_container">
                    <?=$form->textField($model, 'payment_method',array('class'=>'form-control'))?>
                </div>
            </div>
            <div class="col-lg-6" style="padding: 0px;">

                <div class="col-lg-5 input_label">
                    <?=$form->labelEx($model, 'email')?>
                </div>
                <div class="col-lg-7 input_container">
                    <?=$form->textField($model, 'email',array('class'=>'form-control')) ?>
                </div>
                <div class="col-lg-5 input_label">
                    <?=$form->labelEx($model, 'login')?>
                </div>
                <div class="col-lg-7 input_container">
                    <?=$form->textField($model, 'login',array('class'=>'form-control'))?>
                </div>
                <div class="col-lg-5 input_label">
                    <?=$form->labelEx($model, 'password')?>
                </div>
                <div class="col-lg-7 input_container">
                    <?=$form->passwordField($model, 'password',array('class'=>'form-control')) ?>
                </div>
                <div class="col-lg-5 input_label">
                    <?=$form->labelEx($model, 'password2')?>
                </div>
                <div class="col-lg-7 input_container">
                    <?=$form->passwordField($model, 'password2',array('class'=>'form-control')) ?>
                </div>
                <div class="col-lg-5 input_label">
                    <?=$form->labelEx($model, 'active')?>
                </div>
                <div class="col-lg-7 input_container">
                    <?=$form->checkBox($model, 'active',array('class'=>'form-control')) ?>
                </div>
                <?
                if(Yii::app()->user->checkAccess(USER::ROLE_MODERATOR))
                {
                ?>
                <div class="col-lg-5 input_label">
                    <?=$form->labelEx($model, 'role_id')?>
                </div>
                <div class="col-lg-7 input_container">
                    <?=$form->dropDownList($model, 'role_id', array('0'=>'Customer','1'=>'Technician','2'=>'Admin',), array('class' => 'form-control')) ?>
                </div>
                <?
                }
                ?>
                <div class="col-lg-12 input_label" align="center">
                    <?=$form->labelEx($model, 'comment')?>
                </div>
                <div class="col-lg-12 input_container">
                    <?=$form->textarea($model, 'comment',array('class'=>'form-control','rows'=>'7'))?>
                </div>
            </div>
            <div class="col-lg-12 error-block" align="center">
				<?
				if($model->hasErrors()){
					foreach($model->errors as $r){
						echo $r[0].'<br/>';
					}
				}
				?>
            </div>

        </div>
    </div>


<?$this->endWidget();?>