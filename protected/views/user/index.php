<?php
$form = $this->beginWidget('CActiveForm', array(
    'id' => 'user-log-form',
    'enableAjaxValidation' => false,
    'htmlOptions' => array('class' => 'form-horizontal'),
));
?>


    <div class="col-md-offset-4 col-lg-4 col-md-offset-4">
        <div class="col-lg-12">
            <div class="login_header col-sm-12">
                LOGIN
            </div>

        <div class="col-lg-12 login_container">
            <div class="col-lg-12 error-block" align="center">
                <?
				if($model->hasErrors()){
					foreach($model->errors as $r){
						echo $r[0].'<br/>';
					}
				}
				?>
            </div>
            <div class="col-lg-4 input_label">
                <?=$form->labelEx($model, 'login')?>
            </div>
            <div class="col-lg-8 input_container">
                <?=$form->textField($model, 'login',array('class'=>'form-control'))?>
            </div>
            <div class="col-lg-4 input_label">
                <?=$form->labelEx($model, 'password')?>
            </div>
            <div class="col-lg-8 input_container">
                <?=$form->passwordField($model, 'password',array('class'=>'form-control')) ?>
            </div>
            <div class="col-lg-4 input_container">
                <?
                $this->widget('bootstrap.widgets.TbButton', array(
                    'label'=>'Registration',
                    'url'=> Yii::app()->createAbsoluteUrl(Yii::app()->controller->id.'/registration'),
                    'htmlOptions'=>array('class'=>'log_button'),
                ));
                ?>
            </div>
            <div class="col-lg-8 input_container">
                <?
                $this->widget(
                    'bootstrap.widgets.TbButton',
                    array('buttonType' => 'submit',
                        'label' => 'LOGIN',
                        'htmlOptions'=>array('class'=>'reg_button'))
                );
                ?>
            </div>
            </div>
        </div>
    </div>

<?$this->endWidget();?>