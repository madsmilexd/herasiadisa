<?php
if (Yii::app()->user->checkAccess(User::ROLE_MODERATOR)) {
    if (Yii::app()->controller->id == 'user') {

        echo '<div class="col-lg-3">';
        $this->widget('bootstrap.widgets.TbButton', array(
            'label' => 'Repairs',
            'url' => Yii::app()->createAbsoluteUrl('repairs/'),
            'htmlOptions' => array('class' => 'menu_button')
        ));

        echo '</div>';
        echo '<div class="col-lg-3">';

        $this->widget('bootstrap.widgets.TbButton', array(
            'label' => 'Brands',
            'url' => Yii::app()->createAbsoluteUrl('brand/'), //'repairs',
            'htmlOptions' => array('class' => 'menu_button')
        ));

        echo '</div>';
        echo '<div class="col-lg-3">';

        $this->widget('bootstrap.widgets.TbButton', array(
            'label' => 'User Management',
            'url' => Yii::app()->createAbsoluteUrl('repairs'), //'repairs',
            'htmlOptions' => array('class' => 'menu_button')
        ));

        echo '</div>';
        echo '<div class="col-lg-3">';

        $this->widget('bootstrap.widgets.TbButton', array(
            'label' => 'Spair Parts',
            'url' => Yii::app()->createAbsoluteUrl('repairs'), //'repairs',
            'htmlOptions' => array('class' => 'menu_button')
        ));

        echo '</div>';

    }
    if (Yii::app()->controller->id == 'repairs' && Yii::app()->controller->action->id == "index") {


        echo '<div class="col-lg-3">';
        $this->widget('bootstrap.widgets.TbButton', array(
            'label' => 'Active Repairs',
            'url' => '/index.php/repairs/active',
            'htmlOptions' => array('class' => 'menu_button')
        ));

        echo '</div>';
        echo '<div class="col-lg-3">';

        $this->widget('bootstrap.widgets.TbButton', array(
            'label' => 'History',
            'url' => '/index.php/repairs/history',
            'htmlOptions' => array('class' => 'menu_button')
        ));

        echo '</div>';

    }

    if (Yii::app()->controller->id == 'brand'  && Yii::app()->controller->action->id =='index') {

        echo '<div class="col-lg-offset-6 col-lg-3">';
        $this->widget(
            'bootstrap.widgets.TbButton',
            array(
                'label' => 'Back',
                'url' => '/',
                'htmlOptions' => array('class' => 'save_button'))
        );
        echo '</div>';

        echo '<div class="col-lg-3">';
        $this->widget(
            'bootstrap.widgets.TbButton',
            array(
                'label' => 'Add',
                'url' => '/index.php/brand/add',
                'htmlOptions' => array('class' => 'save_button'))
        );
        echo '</div>';
        }

    if(Yii::app()->controller->id == 'brand' && (Yii::app()->controller->action->id == 'edit' || Yii::app()->controller->action->id == 'add'))
    {
        echo '<div class="col-lg-offset-9 col-lg-3">';
        $this->widget(
            'bootstrap.widgets.TbButton',
            array(
                'label' => 'Back',
                'url' => '/index.php/brand/',
                'htmlOptions' => array('class' => 'save_button'))
        );
        echo '</div>';
    }


}