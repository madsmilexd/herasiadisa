<?php
if(Yii::app()->user->checkAccess(User::ROLE_CUSTOMER))
{
    if(Yii::app()->controller->id == 'user')
    {


  ?>
    <div class="col-lg-4">
        <?$this->widget('bootstrap.widgets.TbButton', array(
            'label'=>'New Repair',
            'url'=>Yii::app()->createAbsoluteUrl('repairs/new'),
            'htmlOptions'=>array('class'=>'menu_button')
        ));
        ?>
    </div>
    <div class="col-lg-4">
        <?
        $this->widget('bootstrap.widgets.TbButton', array(
            'label'=>'Repairs',
            'url'=>Yii::app()->createAbsoluteUrl('repairs'),//'repairs',
            'htmlOptions'=>array('class'=>'menu_button')
        ));
        ?>
    </div>
<?
    }
    if(Yii::app()->controller->id == 'repairs')
    {
        ?>

        <div class="col-lg-4">
            <?$this->widget('bootstrap.widgets.TbButton', array(
                'label'=>'Back',
                'url'=>'/',
                'htmlOptions'=>array('class'=>'menu_button')
            ));
            ?>
        </div>
        <div class="col-lg-4">
        <?
        $this->widget('bootstrap.widgets.TbButton', array(
            'label'=>'Print',
            'buttonType' => 'submit',
            'htmlOptions'=>array('class'=>'menu_button')
        ));
        ?>
        </div>
<?

    }
}