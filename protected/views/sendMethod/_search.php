<?php
/* @var $this BrandController */
/* @var $model Brand */
/* @var $form CActiveForm */
?>

<div class="wide form under_header_header_search">

    <?php $form=$this->beginWidget('CActiveForm', array(
        'action'=>Yii::app()->createUrl($this->route),
        'method'=>'get',
    )); ?>
    <div class="col-sm-6">
        <div class="col-sm-3" style="margin-top: 10px">
            <?php echo $form->label($model,'method_id'); ?>
        </div>
        <div class="col-sm-9" style="margin-top: 10px">
            <?php echo $form->textField($model,'method_id', array('class'=>'form-control input-sm')); ?>
        </div>

        <div class="col-sm-3" style="margin-top: 10px">
            <?php echo $form->label($model,'name'); ?>
        </div>
        <div class="col-sm-9" style="margin-top: 10px">
            <?php echo $form->textField($model,'name',array('size'=>60,'maxlength'=>255,'class'=>'form-control input-sm')); ?>
        </div>
    </div>
    <div class="col-sm-6">
        <div class="col-sm-3" style="margin-top: 10px">
            <?php echo $form->label($model,'description'); ?>
        </div>
        <div class="col-sm-9" style="margin-top: 10px">
            <?php echo $form->textArea($model,'description',array('rows'=>3, 'cols'=>50,'class'=>'form-control input-sm')); ?>
        </div>

    </div>

    <div class="col-lg-offset-4 col-sm-4">
        <?
        $this->widget('bootstrap.widgets.TbButton', array(
            'label' => 'Search',
            'buttonType' => 'submit',
            'htmlOptions' => array('class' => 'repair_button')
        ));
        ?>
    </div>
    <?php $this->endWidget(); ?>

</div><!-- search-form -->