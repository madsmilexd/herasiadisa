<?php
/* @var $this SendMethodController */
/* @var $data SendMethod */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('method_id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->method_id), array('view', 'id'=>$data->method_id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('name')); ?>:</b>
	<?php echo CHtml::encode($data->name); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('description')); ?>:</b>
	<?php echo CHtml::encode($data->description); ?>
	<br />


</div>