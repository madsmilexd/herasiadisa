<?php
/* @var $this SendMethodController */
/* @var $model SendMethod */

$this->breadcrumbs=array(
	'Send Methods'=>array('index'),
	$model->name,
);

$this->menu=array(
	array('label'=>'List SendMethod', 'url'=>array('index')),
	array('label'=>'Create SendMethod', 'url'=>array('create')),
	array('label'=>'Update SendMethod', 'url'=>array('update', 'id'=>$model->method_id)),
	array('label'=>'Delete SendMethod', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->method_id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage SendMethod', 'url'=>array('admin')),
);
?>

<h1>View SendMethod #<?php echo $model->method_id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'method_id',
		'name',
		'description',
	),
)); ?>
