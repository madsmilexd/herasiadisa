<div class="col-md-offset-2 col-sm-8 col-md-offset-2">
    <div class="col-sm-12 reg_container">
        <div class="reg_header col-sm-12">
            <div class="col-sm-6">UPDATE SEND METHOD <?php echo $model->method_id; ?></div>
            <div class="col-sm-6" align="right">
                <?
                $this->widget('bootstrap.widgets.TbButton', array(
                    'label' => 'Back',
                    'url' => Yii::app()->createAbsoluteUrl(Yii::app()->controller->id),
                    'htmlOptions' => array('class' => 'menu_button')
                ));
                $this->widget('bootstrap.widgets.TbButton', array(
                    'label' => 'Create Send Method',
                    'url' => Yii::app()->createAbsoluteUrl(Yii::app()->controller->id.'/create'),
                    'htmlOptions' => array('class' => 'menu_button')
                ));
                ?>
            </div>
        </div>
<?php
$this->breadcrumbs=array(
	'Send Methods'=>array('index'),
	$model->name=>array('view','id'=>$model->method_id),
	'Update',
);
?>
<?php $this->renderPartial('_form', array('model'=>$model)); ?>
        </div></div>