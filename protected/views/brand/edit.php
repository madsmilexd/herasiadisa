<?php
$form = $this->beginWidget('CActiveForm', array(
    'id' => 'user-reg-form',
    'enableAjaxValidation' => false,
    'htmlOptions' => array('class' => 'form-horizontal'),
));

?>
    <div class="col-md-offset-2 col-sm-8 col-md-offset-2">
        <div class="col-lg-12 reg_container">
            <div class="reg_header col-sm-12">
                <div class="col-lg-6">BRAND #<?=$model->brand_id?></div>
                <div class="col-lg-6" align="right">
                    <?
                    $this->widget(
                        'bootstrap.widgets.TbButton',
                        array(
                            'label' => 'Back',
                            'url' => Yii::app()->createAbsoluteUrl('brand/'),
                            'htmlOptions' => array('class' => 'menu_button'))
                    );
                    $this->widget(
                        'bootstrap.widgets.TbButton',
                        array(
                            'label' => 'Save',
                            'buttonType' => 'submit',
                            'htmlOptions'=>array('class'=>'menu_button'))
                    );
                    ?>
                </div>
            </div>
    <div class="col-lg-12" style="padding: 0px;">
<div class="col-lg-3 input_label">
            <?=$form->labelEx($model, 'name')?>
        </div>
        <div class="col-lg-7 input_container">
            <?=$form->textField($model, 'name',array('class'=>'form-control'))?>
        </div>
        <div class="col-lg-12 input_label">
            <?=$form->labelEx($model, 'description')?>
        </div>
        <div class="col-lg-12 input_container">
            <?=$form->Textarea($model, 'description',array('class'=>'form-control', 'rows'=>'6')) ?>
        </div>
    </div>
        </div>
    </div>
    </div>


<?$this->endWidget();?>