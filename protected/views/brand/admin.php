<?php
/* @var $this BrandController */
/* @var $model Brand */

?>
<div class="col-md-offset-2 col-sm-8 col-md-offset-2">
    <div class="col-sm-12 reg_container" align="center">
        <div class="reg_header col-sm-12">
            <div class="col-sm-6">MANAGE BRANDS</div>
            <div class="col-sm-6" align="right">
                <?
                $this->widget('bootstrap.widgets.TbButton', array(
                    'label' => 'Create Brand',
                    'url' => Yii::app()->createAbsoluteUrl(Yii::app()->controller->id.'/create'),
                    'htmlOptions' => array('class' => 'menu_button')
                ));

                $this->widget('bootstrap.widgets.TbButton', array(
                    'label' => 'Advanced Search',
                    'url' => '#',
                    'htmlOptions' => array('class' => 'search-button menu_button')
                ));
                ?>
            </div>
        </div>
<?
$this->breadcrumbs=array(
	'Brands'=>array('index'),
	'Manage',
);



Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#brand-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('bootstrap.widgets.TbGridView', array(
	'id'=>'brand-grid',
    'type' => 'striped bordered condensed',
    'dataProvider'=>$model->search(),
    'pager' => array(
        'cssFile'=>'style.css',
        'maxButtonCount'=>'5',
        'header' => '', //'<b>Перейти к странице:</b><br><br>', // заголовок над листалкой
        'prevPageLabel' => 'Prev',
        'nextPageLabel' => 'Next',
        'htmlOptions' => array('class' => 'col-sm-12'),
        'selectedPageCssClass' => 'active'
    ),
//	'filter'=>$model,
	'columns'=>array(
		'brand_id',
		'name',
		'description',
        array(

            'name'=>'brand_id',
            'header' => 'Actions',
            'type' => 'html',
            'value' => function ($data) {
                    return '
                    <a href="'.Yii::app()->createAbsoluteUrl('brand/edit/'.$data->brand_id).'"><span class="glyphicon glyphicon-pencil" style="padding: 5px"></span></a>
                    <a href="'.Yii::app()->createAbsoluteUrl('brand/delete/'.$data->brand_id).'"><span class="glyphicon glyphicon-trash" style="padding: 5px"></span></a>

                    ';
                },

        ),
//		array(
//			'class'=>'CButtonColumn',
//		),
	),
)); ?>
