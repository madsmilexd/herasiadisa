<?php
/* @var $this CrepairController */
/* @var $model Repair */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'repair-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?
	if($model->hasErrors()){
		foreach($model->errors as $r){
			echo $r[0].'<br/>';
		}
	}
	?>

	<div class="row">
		<?php echo $form->labelEx($model,'brand_id'); ?>
		<?php echo $form->textField($model,'brand_id'); ?>
		<?php echo $form->error($model,'brand_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'model'); ?>
		<?php echo $form->textField($model,'model',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'model'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'accessories'); ?>
		<?php echo $form->textField($model,'accessories',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'accessories'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'send_method'); ?>
		<?php echo $form->textField($model,'send_method'); ?>
		<?php echo $form->error($model,'send_method'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'old_service_pswd'); ?>
		<?php echo $form->textField($model,'old_service_pswd',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'old_service_pswd'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'tracking_number'); ?>
		<?php echo $form->textField($model,'tracking_number',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'tracking_number'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'shiping_number'); ?>
		<?php echo $form->textField($model,'shiping_number',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'shiping_number'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'damage'); ?>
		<?php echo $form->textArea($model,'damage',array('rows'=>6, 'cols'=>50)); ?>
		<?php echo $form->error($model,'damage'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'diagnostic'); ?>
		<?php echo $form->textArea($model,'diagnostic',array('rows'=>6, 'cols'=>50)); ?>
		<?php echo $form->error($model,'diagnostic'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'date'); ?>
		<?php echo $form->textField($model,'date'); ?>
		<?php echo $form->error($model,'date'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'client_id'); ?>
		<?php echo $form->textField($model,'client_id'); ?>
		<?php echo $form->error($model,'client_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'support_id'); ?>
		<?php echo $form->textField($model,'support_id'); ?>
		<?php echo $form->error($model,'support_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'status'); ?>
		<?php echo $form->textField($model,'status'); ?>
		<?php echo $form->error($model,'status'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'warehouse_id'); ?>
		<?php echo $form->textField($model,'warehouse_id'); ?>
		<?php echo $form->error($model,'warehouse_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'price'); ?>
		<?php echo $form->textField($model,'price'); ?>
		<?php echo $form->error($model,'price'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->