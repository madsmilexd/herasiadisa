<?php
/* @var $this UsersController */
/* @var $model User */
/* @var $form CActiveForm */
?>
<div class="wide form">
    <?php $form=$this->beginWidget('CActiveForm', array(
        'action'=>Yii::app()->createUrl($this->route),
        'method'=>'get',
    )); ?>
    <div class="col-lg-12" style="padding: 0px;">
        <?
        if(Yii::app()->controller->action->id == 'activeparts' || Yii::app()->controller->id == 'historyparts')
        {?>
        <div class="col-sm-6">
            <div class="row"><div class="col-lg-5 input_label">
                    <?php echo $form->labelEx($model,'accessories'); ?>
                </div><div class="col-lg-7 input_container">
                    <?php echo $form->textField($model,'accessories',array('size'=>60,'maxlength'=>255,'class' => 'form-control')); ?>
                </div></div>

            <div class="row"><div class="col-lg-5 input_label">
                    <?php echo $form->labelEx($model,'repair_id'); ?>
                </div><div class="col-lg-7 input_container">
                    <?php echo $form->textField($model,'repair_id',array('size'=>60,'maxlength'=>255,'class' => 'form-control')); ?>
                </div></div>

        <div class="row"><div class="col-lg-5 input_label">
                <?php echo $form->labelEx($model,'damage'); ?>
            </div><div class="col-lg-7 input_container">
                <?php echo $form->textField($model,'damage',array('size'=>60,'maxlength'=>255,'class' => 'form-control')); ?>
            </div></div>

        <div class="row"><div class="col-lg-5 input_label">
                <?php echo $form->labelEx($model,'diagnostic'); ?>
            </div><div class="col-lg-7 input_container">
                <?php echo $form->textField($model,'diagnostic',array('size'=>60,'maxlength'=>255,'class' => 'form-control')); ?>
            </div></div>

            <div class="row"><div class="col-lg-5 input_label">
                    <?php echo $form->labelEx($model,'price'); ?>
                </div><div class="col-lg-7 input_container">
                    <?php echo $form->passwordField($model,'price',array('size'=>60,'maxlength'=>255,'class' => 'form-control')); ?>
                </div></div>

        <?
        }else{
        ?>
        <div class="col-sm-6">
            <div class="row"><div class="col-lg-5 input_label">
                    <?php echo $form->labelEx($model,'repair_id'); ?>
                </div><div class="col-lg-7 input_container">
                    <?php echo $form->textField($model,'repair_id',array('size'=>60,'maxlength'=>255,'class' => 'form-control')); ?>
                </div></div>

            <div class="row"><div class="col-lg-5 input_label">
                    <?php echo $form->labelEx($model,'brand_id'); ?>
                </div><div class="col-lg-7 input_container">
                    <?php echo $form->textField($model,'brand_id',array('size'=>60,'maxlength'=>255,'class' => 'form-control')); ?>
                </div></div>

            <div class="row"><div class="col-lg-5 input_label">
                    <?php echo $form->labelEx($model,'model'); ?>
                </div><div class="col-lg-7 input_container">
                    <?php echo $form->textField($model,'model',array('size'=>60,'maxlength'=>255,'class' => 'form-control')); ?>
                </div></div>

            <div class="row"><div class="col-lg-5 input_label">
                    <?php echo $form->labelEx($model,'accessories'); ?>
                </div><div class="col-lg-7 input_container">
                    <?php echo $form->textField($model,'accessories',array('size'=>60,'maxlength'=>255,'class' => 'form-control')); ?>
                </div></div>

            <div class="row"><div class="col-lg-5 input_label">
                    <?php echo $form->labelEx($model,'send_method'); ?>
                </div><div class="col-lg-7 input_container">
                    <?php echo $form->textField($model,'send_method',array('size'=>60,'maxlength'=>255,'class' => 'form-control')); ?>
                </div></div>

            <div class="row"><div class="col-lg-5 input_label">
                    <?php echo $form->labelEx($model,'old_service_pswd'); ?>
                </div><div class="col-lg-7 input_container">
                    <?php echo $form->textField($model,'old_service_pswd',array('size'=>60,'maxlength'=>255,'class' => 'form-control')); ?>
                </div></div>

            <div class="row"><div class="col-lg-5 input_label">
                    <?php echo $form->labelEx($model,'tracking_number'); ?>
                </div><div class="col-lg-7 input_container">
                    <?php echo $form->textField($model,'tracking_number',array('size'=>60,'maxlength'=>255,'class' => 'form-control')); ?>
                </div></div>

            <div class="row"><div class="col-lg-5 input_label">
                    <?php echo $form->labelEx($model,'shiping_number'); ?>
                </div><div class="col-lg-7 input_container">
                    <?php echo $form->textField($model,'shiping_number',array('size'=>60,'maxlength'=>255,'class' => 'form-control')); ?>
                </div></div>

            <div class="row"><div class="col-lg-5 input_label">
                    <?php echo $form->labelEx($model,'damage'); ?>
                </div><div class="col-lg-7 input_container">
                    <?php echo $form->textField($model,'damage',array('size'=>60,'maxlength'=>255,'class' => 'form-control')); ?>
                </div></div>
        </div><div class="col-sm-6">
            <div class="row"><div class="col-lg-5 input_label">
                    <?php echo $form->labelEx($model,'diagnostic'); ?>
                </div><div class="col-lg-7 input_container">
                    <?php echo $form->textField($model,'diagnostic',array('size'=>60,'maxlength'=>255,'class' => 'form-control')); ?>
                </div></div>

            <div class="row"><div class="col-lg-5 input_label">
                    <?php echo $form->labelEx($model,'client_id'); ?>
                </div><div class="col-lg-7 input_container">
                    <?php echo $form->textField($model,'client_id',array('size'=>60,'maxlength'=>255,'class' => 'form-control')); ?>
                </div></div>

            <div class="row"><div class="col-lg-5 input_label">
                    <?php echo $form->labelEx($model,'support_id'); ?>
                </div><div class="col-lg-7 input_container">
                    <?php echo $form->textField($model,'support_id',array('size'=>60,'maxlength'=>255,'class' => 'form-control')); ?>
                </div></div>

            <div class="row"><div class="col-lg-5 input_label">
                    <?php echo $form->labelEx($model,'status'); ?>
                </div><div class="col-lg-7 input_container">
                    <?php echo $form->textField($model,'status',array('size'=>60,'maxlength'=>255,'class' => 'form-control')); ?>
                </div></div>

            <div class="row"><div class="col-lg-5 input_label">
                    <?php echo $form->labelEx($model,'price'); ?>
                </div><div class="col-lg-7 input_container">
                    <?php echo $form->passwordField($model,'price',array('size'=>60,'maxlength'=>255,'class' => 'form-control')); ?>
                </div></div>

            <?
            }
            $this->widget(
                'bootstrap.widgets.TbButton',
                array('buttonType' => 'submit',
                    'label' => 'Search',
                    'htmlOptions' => array('class' => 'repair_button'))
            );

            ?>
        </div></div>
    </div>
<?php $this->endWidget(); ?>
