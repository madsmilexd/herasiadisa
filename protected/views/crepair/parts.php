<div class="col-lg-offset-2 col-sm-8">
    <div class="col-sm-12 reg_container" align="center">
        <div class="reg_header col-sm-12">
            <div class="col-sm-4">MANAGE PARTS</div>
            <div class="col-sm-5">
                <?
                $this->widget('bootstrap.widgets.TbButton', array(
                    'label' => 'Active',
                    'url' => Yii::app()->createAbsoluteUrl(Yii::app()->controller->id.'/activeparts'),
                    'htmlOptions' => array('class' => 'menu_button')
                ));
                $this->widget('bootstrap.widgets.TbButton', array(
                    'label' => 'History',
                    'url' => Yii::app()->createAbsoluteUrl(Yii::app()->controller->id.'/historyparts'),
                    'htmlOptions' => array('class' => 'menu_button')
                ));
                $this->widget('bootstrap.widgets.TbButton', array(
                    'label' => 'Create Order',
                    'url' => Yii::app()->createAbsoluteUrl('repairs/newpart'),
                    'htmlOptions' => array('class' => 'menu_button')
                ));

                ?>
            </div>
            <div class="col-sm-3" align="right">


                <div class="input-group">
                    <?php echo CHtml::textField('inpSearch', '', array('class'=>'form-control','placeholder'=>'Serach...')); ?>
                    <span class="input-group-addon">
<span class="glyphicon glyphicon-search"></span>
</span>
                </div>
            </div>
        </div>

<?php
/* @var $this CrepairController */
/* @var $model Repair */

$this->breadcrumbs=array(
    'Repairs'=>array('index'),
    'Manage',
);
Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#repair-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>
        <?php
		Yii::app()->clientScript->registerScript('search', "
  $('input#inpSearch').keyup(function(){
  $.fn.yiiGridView.update('repair-grid', {
  data: $(this).serialize()
  });
  return false;
});
");
		?>
        <div class="col-sm-12">

<?php $this->widget('bootstrap.widgets.TbGridView', array(
    'id'=>'repair-grid',
    'type' => 'striped bordered condensed',
    'dataProvider'=>$model->search(),
    'pager' => array(
        'cssFile'=>'style.css',
        'maxButtonCount'=>'5',
        'header' => '', //'<b>Перейти к странице:</b><br><br>', // заголовок над листалкой
        'prevPageLabel' => 'Prev',
        'nextPageLabel' => 'Next',
        'htmlOptions' => array('class' => 'col-sm-12'),
        'selectedPageCssClass' => 'active'
    ),
//	'filter'=>$model,
    'columns'=>array(
		array(
			'name'=>'repair_id',
			'header'=>'ID',
			'value'=> function($data){
					$d = explode('/',$data->date,1);
					$t = $d[0];
					$r = $t[2].$t[3];
					return '#'.$data->repair_id.'/'.$r;}
		),
        array(
            'name'=>'client_id',
            'header'=>'User Name',
            'value'=> function($data){return $data->client->login;}
        ),
        'date',
        array(
            'name'=>'damage',
            'header'=>'Type',
        ),
        array(
            'name'=>'diagnostic',
            'header'=>'Order Description',
        ),
//        'diagnostic',
        array(
            'name'=>'status',
            'header'=>'
                <div class="dropdown col-sm-12 dropdown_header">
    <a class="btn btn-default dropdown-toggle col-sm-12" data-toggle="dropdown" href="#">
        Status
        <span class="caret"></span>
    </a>
    <ul class="dropdown-menu">
                        <li
                        role="presentation"><a role="menuitem" tabindex="-1" href="?Repair[status]=0">New order</a></li>
                        <li role="presentation"><a role="menuitem" tabindex="-1" href="?Repair[status]=1">Waiting
                         for Diagnostic</a></li>
                        <li role="presentation"><a role="menuitem" tabindex="-1" href="?Repair[status]=2">Waiting for
                         customer agreement</a></li>

                         <li role="presentation"><a role="menuitem" tabindex="-1" href="?Repair[status]=3">Sent for repair</a></li>
                         <li role="presentation"><a role="menuitem" tabindex="-1" href="?Repair[status]=4">Waiting for spair parts</a></li>
                         <li role="presentation"><a role="menuitem" tabindex="-1" href="?Repair[status]=5">Spare parts derived</a></li>
                         <li role="presentation"><a role="menuitem" tabindex="-1" href="?Repair[status]=6">Successfully completed</a></li>
                         <li role="presentation"><a role="menuitem" tabindex="-1" href="?Repair[status]=7">Completed unsuccessfully</a></li>
                         <li role="presentation"><a role="menuitem" tabindex="-1" href="?Repair[status]=8">Canceled by customer</a></li>

                        <li role="presentation" class="divider"></li>
                        <li role="presentation"><a role="menuitem" tabindex="-1" href="?Repair[status]">All Repairs
                                                </a></li>
    </ul>
</div>
						',
            'htmlOptions' => array('style' => 'text-align:center;vertical-align: middle;'),
            'type' => 'html',
            'value'=> function($data){
                    $model =Colors::model()->getStatus($data->status);
                    $a = '<div style="background-color:'.$model->color.';">'.$model->description.'</div>';
                    return $a;
                }
        ),
        'price',
        array(
            'name'=>'repair_id',
            'header' => 'Actions',
            'type' => 'html',
            'value' => function ($data) {
                    return '<a href="'.Yii::app()->createAbsoluteUrl('repairs/partsdetails/'.$data->repair_id)
						.'"><span class="glyphicon
					glyphicon-cog" style="padding: 5px"></span></a>';
                },

        ),
    ),
)); ?>
</div></div></div>