<?php
/* @var $this CrepairController */
/* @var $model Repair */

$this->breadcrumbs=array(
	'Repairs'=>array('index'),
	$model->repair_id,
);

$this->menu=array(
	array('label'=>'List Repair', 'url'=>array('index')),
	array('label'=>'Create Repair', 'url'=>array('create')),
	array('label'=>'Update Repair', 'url'=>array('update', 'id'=>$model->repair_id)),
	array('label'=>'Delete Repair', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->repair_id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Repair', 'url'=>array('admin')),
);
?>

<h1>View Repair #<?php echo $model->repair_id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'repair_id',
		'brand_id',
		'model',
		'accessories',
		'send_method',
		'old_service_pswd',
		'tracking_number',
		'shiping_number',
		'damage',
		'diagnostic',
		'date',
		'client_id',
		'support_id',
		array(
			'name'=>'status',
			'type'=>'raw',
			'value'=>function($data){
					return $data->colors->color;
				}
		),
		'warehouse_id',
		'price',
	),
)); ?>
