<?php
/* @var $this CrepairController */
/* @var $data Repair */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('repair_id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->repair_id), array('view', 'id'=>$data->repair_id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('brand_id')); ?>:</b>
	<?php echo CHtml::encode($data->brand_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('model')); ?>:</b>
	<?php echo CHtml::encode($data->model); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('accessories')); ?>:</b>
	<?php echo CHtml::encode($data->accessories); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('send_method')); ?>:</b>
	<?php echo CHtml::encode($data->send_method); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('old_service_pswd')); ?>:</b>
	<?php echo CHtml::encode($data->old_service_pswd); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('tracking_number')); ?>:</b>
	<?php echo CHtml::encode($data->tracking_number); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('shiping_number')); ?>:</b>
	<?php echo CHtml::encode($data->shiping_number); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('damage')); ?>:</b>
	<?php echo CHtml::encode($data->damage); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('Diagnostic')); ?>:</b>
	<?php echo CHtml::encode($data->Diagnostic); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('date')); ?>:</b>
	<?php echo CHtml::encode($data->date); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('client_id')); ?>:</b>
	<?php echo CHtml::encode($data->client_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('support_id')); ?>:</b>
	<?php echo CHtml::encode($data->support_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('status')); ?>:</b>
	<?php echo CHtml::encode($data->status); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('warehouse_id')); ?>:</b>
	<?php echo CHtml::encode($data->warehouse_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('price')); ?>:</b>
	<?php echo CHtml::encode($data->price); ?>
	<br />

	*/ ?>

</div>