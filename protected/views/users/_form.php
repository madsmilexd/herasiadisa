<?php
/* @var $this UsersController */
/* @var $model User */
/* @var $form CActiveForm */
?>
<div class="col-md-offset-2 col-sm-8 col-md-offset-2">
<div class="form">
    <div class="col-lg-12" style="padding: 0px;">
<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'user-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>

		<?
		if($model->hasErrors()){
			foreach($model->errors as $r){
				echo $r[0].'<br/>';
			}
		}
		?>

	<div class="row"><div class="col-lg-5 input_label">
		<?php echo $form->labelEx($model,'first_name'); ?>
        </div><div class="col-lg-7 input_container">
		<?php echo $form->textField($model,'first_name',array('size'=>60,'maxlength'=>255,'class' => 'form-control')); ?>
		<?php echo $form->error($model,'first_name'); ?>
	</div></div>

	<div class="row"><div class="col-lg-5 input_label">
		<?php echo $form->labelEx($model,'last_name'); ?>
        </div><div class="col-lg-7 input_container">
		<?php echo $form->textField($model,'last_name',array('size'=>60,'maxlength'=>255,'class' => 'form-control')); ?>
		<?php echo $form->error($model,'last_name'); ?>
	</div></div>

	<div class="row"><div class="col-lg-5 input_label">
		<?php echo $form->labelEx($model,'address'); ?>
        </div><div class="col-lg-7 input_container">
		<?php echo $form->textField($model,'address',array('size'=>60,'maxlength'=>255,'class' => 'form-control')); ?>
		<?php echo $form->error($model,'address'); ?>
	</div></div>

	<div class="row"><div class="col-lg-5 input_label">
		<?php echo $form->labelEx($model,'city'); ?>
        </div><div class="col-lg-7 input_container">
		<?php echo $form->textField($model,'city',array('size'=>60,'maxlength'=>255,'class' => 'form-control')); ?>
		<?php echo $form->error($model,'city'); ?>
	</div></div>

	<div class="row"><div class="col-lg-5 input_label">
		<?php echo $form->labelEx($model,'phone_numb'); ?>
        </div><div class="col-lg-7 input_container">
		<?php echo $form->textField($model,'phone_numb',array('size'=>60,'maxlength'=>255,'class' => 'form-control')); ?>
		<?php echo $form->error($model,'phone_numb'); ?>
	</div></div>

	<div class="row"><div class="col-lg-5 input_label">
		<?php echo $form->labelEx($model,'cell_phone'); ?>
        </div><div class="col-lg-7 input_container">
		<?php echo $form->textField($model,'cell_phone',array('size'=>60,'maxlength'=>255,'class' => 'form-control')); ?>
		<?php echo $form->error($model,'cell_phone'); ?>
	</div></div>

	<div class="row"><div class="col-lg-5 input_label">
		<?php echo $form->labelEx($model,'company_name'); ?>
        </div><div class="col-lg-7 input_container">
		<?php echo $form->textField($model,'company_name',array('size'=>60,'maxlength'=>255,'class' => 'form-control')); ?>
		<?php echo $form->error($model,'company_name'); ?>
	</div></div>

	<div class="row"><div class="col-lg-5 input_label">
		<?php echo $form->labelEx($model,'profession'); ?>
        </div><div class="col-lg-7 input_container">
		<?php echo $form->textField($model,'profession',array('size'=>60,'maxlength'=>255,'class' => 'form-control')); ?>
		<?php echo $form->error($model,'profession'); ?>
	</div></div>

	<div class="row"><div class="col-lg-5 input_label">
		<?php echo $form->labelEx($model,'tax_agency'); ?>
        </div><div class="col-lg-7 input_container">
		<?php echo $form->textField($model,'tax_agency',array('size'=>60,'maxlength'=>255,'class' => 'form-control')); ?>
		<?php echo $form->error($model,'tax_agency'); ?>
	</div></div>

	<div class="row"><div class="col-lg-5 input_label">
		<?php echo $form->labelEx($model,'vat_number'); ?>
        </div><div class="col-lg-7 input_container">
		<?php echo $form->textField($model,'vat_number',array('size'=>60,'maxlength'=>255,'class' => 'form-control')); ?>
		<?php echo $form->error($model,'vat_number'); ?>
	</div></div>

	<div class="row"><div class="col-lg-5 input_label">
		<?php echo $form->labelEx($model,'payment_method'); ?>
        </div><div class="col-lg-7 input_container">
		<?php echo $form->textField($model,'payment_method',array('size'=>60,'maxlength'=>255,'class' => 'form-control')); ?>
		<?php echo $form->error($model,'payment_method'); ?>
	</div></div>

	<div class="row"><div class="col-lg-5 input_label">
		<?php echo $form->labelEx($model,'email'); ?>
        </div><div class="col-lg-7 input_container">
		<?php echo $form->textField($model,'email',array('size'=>60,'maxlength'=>255,'class' => 'form-control')); ?>
		<?php echo $form->error($model,'email'); ?>
	</div></div>

	<div class="row"><div class="col-lg-5 input_label">
		<?php echo $form->labelEx($model,'login'); ?>
        </div><div class="col-lg-7 input_container">
		<?php echo $form->textField($model,'login',array('size'=>60,'maxlength'=>255,'class' => 'form-control')); ?>
		<?php echo $form->error($model,'login'); ?>
	</div></div>

	<div class="row"><div class="col-lg-5 input_label">
		<?php echo $form->labelEx($model,'password'); ?>
        </div><div class="col-lg-7 input_container">
		<?php echo $form->passwordField($model,'password',array('size'=>60,'maxlength'=>255,'class' => 'form-control')); ?>
		<?php echo $form->error($model,'password'); ?>
	</div></div>

	<div class="row">
        <div class="col-lg-5 input_label">
            <?=$form->labelEx($model, 'role_id')?>
        </div>
        <div class="col-lg-7 input_container">
            <?=$form->dropDownList($model, 'role_id', array(User::ROLE_CUSTOMER=>'Customer',User::ROLE_MODERATOR=>'Technician',User::ROLE_ADMIN=>'Admin'), array('class' => 'form-control')) ?>
            <?php echo $form->error($model,'role_id'); ?>
        </div>
     </div>
        <div class="row">
            <div class="col-lg-5 input_label">
                <?=$form->labelEx($model, 'active')?>
            </div>
            <div class="col-lg-7 input_container">
                <?=$form->dropDownList($model, 'active', array(User::ACTIVE_ME=>'New',User::ACTIVE_TRUE=>'Active',User::WAITING_FOR_EMAIL_ACTIVIZATION=>'Waiting for email activation', User::ACTIVE_BANNED=>'Banned'), array('class' => 'form-control')) ?>
                <?php echo $form->error($model,'active'); ?>
            </div>
        </div>
	<div class="row"><div class="col-lg-5 input_label">
		<?php echo $form->labelEx($model,'comment'); ?>
        </div><div class="col-lg-7 input_container">
		<?php echo $form->textField($model,'comment',array('size'=>60,'maxlength'=>255,'class' => 'form-control')); ?>
		<?php echo $form->error($model,'comment'); ?>
	</div></div>

        <?
        $this->widget(
            'bootstrap.widgets.TbButton',
            array('buttonType' => 'submit',
                'label' => 'Save',
                'htmlOptions' => array('class' => 'repair_button'))
        );
        ?>
</div>
    </div>
<?php $this->endWidget(); ?>

</div><!-- form -->