<div class="col-lg-offset-2 col-sm-8">
	<div class="col-sm-12 reg_container">
		<div class="reg_header col-sm-12">
			<div class="col-sm-3">MANAGE USERS</div>
                <div class="col-sm-2">
                    </div>
            <div class="col-sm-4">
            <?
				$this->widget('bootstrap.widgets.TbButton', array(
					'label' => 'Back',
					'url' => Yii::app()->createAbsoluteUrl(Yii::app()->controller->id),
					'htmlOptions' => array('class' => 'menu_button')
				));
				$this->widget('bootstrap.widgets.TbButton', array(
					'label' => 'New User',
					'url' => Yii::app()->createAbsoluteUrl(Yii::app()->controller->id . '/create'),
					'htmlOptions' => array('class' => 'menu_button')
				));
                ?>
                </div>
            <div class="col-sm-3" align="right">


            <div class="input-group">
            <?php echo CHtml::textField('inpSearch', '', array('class'=>'form-control','placeholder'=>'Serach...')  ); ?>
                <span class="input-group-addon">
<span class="glyphicon glyphicon-search"></span>
</span>
            </div>
			</div>
		</div>
		<?php
		Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#user-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");


		?>
<div class="col-sm-12">




		<?php

		Yii::app()->clientScript->registerScript('search', "
  $('input#inpSearch').keyup(function(){
  $.fn.yiiGridView.update('user-grid', {
  data: $(this).serialize()
  });
  return false;
});
");
		?>

		<div id="users_list" align="center">
			<?
			$this->widget('bootstrap.widgets.TbGridView', array(
				'id' => 'user-grid',
				'type' => 'striped bordered condensed',
				'pager' => array(
                    'cssFile'=>'style.css',
                    'maxButtonCount'=>'5',
					'header' => '', //'<b>Перейти к странице:</b><br><br>', // заголовок над листалкой
					'prevPageLabel' => 'Prev',
					'nextPageLabel' => 'Next',
					'htmlOptions' => array('class' => 'col-sm-12'),
					'selectedPageCssClass' => 'active',



				),
				'dataProvider' =>$model->search(),
				'columns' => array(
					'id',
					array(
						'name' => 'a',
						'header' => 'User Name',
						'value' => function ($data) {
								$name = $data->first_name . ' ' . $data->last_name;
								return $name;
							},
					),
					'login',
					'email',
					array(
						'name' => 'role_id',
						'header' => '
                <div class="dropdown col-sm-12 dropdown_header">
    <a class="btn btn-default dropdown-toggle col-sm-12" data-toggle="dropdown" href="#">
        Role
        <span class="caret"></span>
    </a>
    <ul class="dropdown-menu">
                        <li role="presentation"><a role="menuitem" tabindex="-1" href="?User[role_id]=0">Customer</a></li>
                        <li role="presentation"><a role="menuitem" tabindex="-1" href="?User[role_id]=1">Moderator</a></li>
                        <li role="presentation"><a role="menuitem" tabindex="-1" href="?User[role_id]=2">Admin</a></li>
                        <li role="presentation" class="divider"></li>
                        <li role="presentation"><a role="menuitem" tabindex="-1" href="?User[role_id]">All users</a></li>
    </ul>
</div>
						',
						'htmlOptions' => array('style' => 'width:30px;text-align:center;vertical-align: middle;'),
						'type' => 'html',
						'value' => function ($data) {
								if ($data->role_id == 0)
									return '<span class="label label-success" style="padding: 5px">Customer</span>';
								if ($data->role_id == 1)
									return '<span class="label label-warning" style="padding: 5px">Moderator</span>';
								if ($data->role_id == 2)
									return '<span class="label label-danger" style="padding: 5px">Admin</span>';

							},
					),
					array(
						'name' => 'active',
						'header' => '

						                <div class="dropdown col-sm-12 dropdown_header">
    <a class="btn btn-default dropdown-toggle col-sm-12" data-toggle="dropdown" href="#">
        Status
        <span class="caret"></span>
    </a>
    <ul class="dropdown-menu">
                        <li role="presentation"><a role="menuitem" tabindex="-1" href="?User[active]=0">Banned</a></li>
                        <li role="presentation"><a role="menuitem" tabindex="-1" href="?User[active]=1">Active</a></li>
                        <li role="presentation"><a role="menuitem" tabindex="-1" href="?User[active]=2">New</a></li>
                        <li role="presentation"><a role="menuitem" tabindex="-1" href="?User[active]=3">Need email confirm</a></li>
                        <li role="presentation" class="divider"></li>
                        <li role="presentation"><a role="menuitem" tabindex="-1" href="?User[role_id]">All users</a></li>
    </ul>
</div>

						',
						'htmlOptions' => array('style' => 'width:150px;text-align:center'),
						'type' => 'html',
						'value' => function ($data) {

								if ($data->active == User::ACTIVE_TRUE) {
									$title = 'Active';
								}
								if ($data->active == User::WAITING_FOR_EMAIL_ACTIVIZATION) {
									$title = 'Need email confirm';
								}
								if ($data->active == User::ACTIVE_BANNED) {
									$title = 'Banned';
								}
								if ($data->active == User::ACTIVE_ME) {
									$title = 'New';
								}
								return $title;
							}
					),
					array(
						'header' => 'Actions',
						'class' => 'CButtonColumn',
						'template' => '{update}',
						'buttons' => array
						(
							'update' => array
							(
								'imageUrl' => false,
								'label' => '<span class="glyphicon glyphicon-pencil"></span>',
								'options' => array('title' => 'View'),
							),
						),
					),
				),

			)); ?>
        </div>
		</div>
	</div>
</div>