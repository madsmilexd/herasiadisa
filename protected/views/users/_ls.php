<?
$this->widget('bootstrap.widgets.TbGridView', array(
	'id' => 'user-grid',
	'type' => 'striped bordered condensed',
	'pager' => array(
		'header' => ' ', //'<b>Перейти к странице:</b><br><br>', // заголовок над листалкой
		'firstPageLabel' => '<div style="height:17px;width:20px" class="glyphicon glyphicon-fast-backward"></div>',
		'prevPageLabel' => '<div style="height:17px;width:20px"  class="glyphicon glyphicon-backward"></div>',
		'nextPageLabel' => '<div style="height:17px;width:20px" class="glyphicon glyphicon-forward"></div>',
		'lastPageLabel' => '<div style="height:17px;width:20px" class="glyphicon glyphicon-fast-forward"></div>',
		'htmlOptions' => array('class' => 'pagination pagination-sm', 'id' => 'adminPaginator'),
		'selectedPageCssClass' => 'active'
	),
	'dataProvider' => $dataProvider,
	'columns' => array(
		'id',
		array(
			'name' => 'a',
			'header' => 'User Name',
			'value' => function ($data) {
					$name = $data->first_name . ' ' . $data->last_name;
					return $name;
				},
		),
		'login',
		'email',
		array(
			'name' => 'role_id',
			'header' => 'Role',
			'htmlOptions' => array('style' => 'width:30px;text-align:center;vertical-align: middle;'),
			'type' => 'html',
			'value' => function ($data) {
					if ($data->role_id == 0)
						return '<span class="label label-success" style="padding: 5px">Customer</span>';
					if ($data->role_id == 1)
						return '<span class="label label-warning" style="padding: 5px">Moderator</span>';
					if ($data->role_id == 2)
						return '<span class="label label-danger" style="padding: 5px">Admin</span>';

				},
		),
		array(
			'name' => 'active',
			'header' => 'Active',
			'htmlOptions' => array('style' => 'width:150px;text-align:center'),
			'type' => 'html',
			'value' => function ($data) {

					if ($data->active == User::ACTIVE_TRUE) {
						$title = 'Active';
					}
					if ($data->active == User::WAITING_FOR_EMAIL_ACTIVIZATION) {
						$title = 'Need email confirm';
					}
					if ($data->active == User::ACTIVE_BANNED) {
						$title = 'Banned';
					}
					if ($data->active == User::ACTIVE_ME) {
						$title = 'New';
					}
					return $title;
				}
		),
		array(
			'header' => 'Actions',
			'class' => 'CButtonColumn',
			'template' => '{update}{delete}',
			'buttons' => array
			(
				'update' => array
				(
					'imageUrl' => false,
					'label' => '<span class="glyphicon glyphicon-pencil"></span>',
					'options' => array('title' => 'View'),
				),
				'delete' => array
				(
					'imageUrl' => false,
					'label' => '<span class="glyphicon glyphicon-trash"></span>',
					'options' => array('title' => 'Delete'),
				),
			),
		),
	),

)); ?>