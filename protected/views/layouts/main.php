<?php /* @var Controller $this */ ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
	<meta name="language" content="en"/>

	<?php
	Yii::app()->getClientScript()->registerCoreScript('jquery');

	$this->cs->registerCssFile(Yii::app()->request->baseUrl . '/css/bscomb.css');
	$this->cs->registerCssFile(Yii::app()->request->baseUrl . '/css/style.css');

	$this->cs->registerCssFile(Yii::app()->request->baseUrl . '/vendors/bootstrap/css/bootstrap.min.css');
	$this->cs->registerCssFile(Yii::app()->request->baseUrl . '/vendors/bootstrap/css/bootstrap-theme.css');
//	$this->cs->registerScriptFile(Yii::app()->request->baseUrl . '/vendors/jquery/jquery-2.1.0.min.js', CClientScript::POS_END);
	$this->cs->registerScriptFile(Yii::app()->request->baseUrl . '/vendors/bootstrap/js/bootstrap.min.js', CClientScript::POS_END);
	$this->cs->registerScriptFile(Yii::app()->request->baseUrl . '/js/html2canvas.js', CClientScript::POS_END);
	$this->cs->registerScriptFile(Yii::app()->request->baseUrl . '/js/pdf.js', CClientScript::POS_END);
	$this->cs->registerScriptFile(Yii::app()->request->baseUrl . '/js/app.js', CClientScript::POS_END);
	$this->cs->registerScriptFile(Yii::app()->request->baseUrl . '/js/drop.js', CClientScript::POS_END);
	$this->cs->registerScriptFile(Yii::app()->request->baseUrl . '/js/growl.js', CClientScript::POS_END);
	$this->cs->registerScriptFile(Yii::app()->request->baseUrl . '/js/acp.js', CClientScript::POS_END);
	$this->cs->registerScriptFile(Yii::app()->request->baseUrl . '/vendors/colorpicker/js/colpick.js',
		CClientScript::POS_END);
	$this->cs->registerCssFile(Yii::app()->request->baseUrl . '/vendors/colorpicker/css/colpick.css');
	if(Yii::app()->user->checkAccess(User::ROLE_MODERATOR)){
		$this->cs->registerScriptFile(Yii::app()->request->baseUrl . '/js/support.js', CClientScript::POS_END);
	}
	echo '<input type="hidden" id="controller_id" value="'.Yii::app()->controller->id.'"/>';
	echo '<input type="hidden" id="action_id" value="'.Yii::app()->controller->action->id.'"/>';
	if(Yii::app()->user->isGuest&&Yii::app()->controller->id!='user'){
		$this->redirect(Yii::app()->createAbsoluteUrl('/'));
	}
	?>

	<title><?= CHtml::encode($this->pageTitle); ?></title>
</head>

<body>
<? include_once('notifications.php') ?>


<div class="col-lg-12 main" align="center">
    <? if(!Yii::app()->user->isGuest){
        include_once('header.php');
    }
     ?>
	<div id="logo"></div>
<?= $content; ?>

	<input id="base_url" style="display:none;" value="<?=Yii::app()->createAbsoluteUrl('user/logout');?>"/>
</div>

</body>
</html>
