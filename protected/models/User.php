<?php

/**
 * This is the model class for table "users".
 *
 * The followings are the available columns in table 'users':
 * @property integer $id
 * @property string $first_name
 * @property string $last_name
 * @property string $address
 * @property string $city
 * @property string $phone_numb
 * @property string $cell_phone
 * @property string $company_name
 * @property string $profession
 * @property string $tax_agency
 * @property string $vat_number
 * @property string $payment_method
 * @property string $email
 * @property string $login
 * @property string $password
 * @property integer $active
 * @property integer $role_id
 * @property string $comment
 * @property string $date
 *
 * The followings are the available model relations:
 * @property Message[] $messages
 * @property Message[] $messages1
 * @property Repair[] $repairs_client
 * @property Repair[] $repairs_support
 */
class User extends CActiveRecord
{
	public $oldstatus;
    public $password2;

	public $kw;
    const ACTIVE_BANNED = 0;
    const ACTIVE_TRUE = 1;
    const ACTIVE_ME =2;
    const WAITING_FOR_EMAIL_ACTIVIZATION = 3;
    const ROLE_ADMIN = 2;
    const ROLE_MODERATOR = 1;
    const ROLE_CUSTOMER = 0;
    /**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		App::model()->ini();
		return 'users';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
            array('login, password', 'required', 'on' => 'login'),
            array('login,email', 'unique', 'on' => 'registration'),
			array('email','email'),
            array('first_name, last_name, address, city, phone_numb, cell_phone, company_name, profession, tax_agency, vat_number, email, login, password', 'required', 'on' => array('create','editUser')),
            array('login,email', 'unique', 'on' => array('create','editUser')),
            array('password', 'compare', 'compareAttribute' => 'password2', 'on' => 'registration'),
			array('first_name, last_name, address, city, phone_numb, cell_phone, company_name, profession, tax_agency, vat_number, email, login, password, password2', 'required', 'on' => array('registration')),
			array('active, role_id', 'numerical', 'integerOnly'=>true),
			array('first_name, last_name, address, city, phone_numb, cell_phone, company_name, profession, tax_agency, vat_number, payment_method, email, login, password, comment', 'length', 'max'=>255),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, first_name, last_name, address, city, phone_numb, cell_phone, company_name, profession, tax_agency, vat_number, payment_method, email, login, password, active, role_id, comment, date', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'messages' => array(self::HAS_MANY, 'Message', 'from_id'),
			'messages1' => array(self::HAS_MANY, 'Message', 'to_id'),
			'repairs_client' => array(self::HAS_MANY, 'Repair', 'client_id'),
			'repairs_support' => array(self::HAS_MANY, 'Repair', 'support_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'first_name' => 'Name',
			'last_name' => 'Last Name',
			'address' => 'Address',
			'city' => 'City P.C.',
			'phone_numb' => 'Phone Number',
			'cell_phone' => 'Cell Phone',
			'company_name' => 'Company Name',
            'profession'=> 'Profession',
			'tax_agency' => 'Tax Agency',
			'vat_number' => 'Vat Number',
			'payment_method' => 'Payment Method',
			'email' => 'Email',
			'login' => 'Login',
			'password' => 'Password',
			'password2' => 'Repeat password',
			'active' => 'Active',
            'role_id' => 'Role ID',
			'comment' => 'Comment',
			'date' => 'Date',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('first_name',$this->first_name,true);
		$criteria->compare('last_name',$this->last_name,true);
		$criteria->compare('address',$this->address,true);
		$criteria->compare('city',$this->city,true);
		$criteria->compare('phone_numb',$this->phone_numb,true);
		$criteria->compare('cell_phone',$this->cell_phone,true);
		$criteria->compare('company_name',$this->company_name,true);
		$criteria->compare('profession',$this->profession,true);
		$criteria->compare('tax_agency',$this->tax_agency,true);
		$criteria->compare('vat_number',$this->vat_number,true);
		$criteria->compare('payment_method',$this->payment_method,true);
		$criteria->compare('email',$this->email,true);
		$criteria->compare('login',$this->login,true);
		$criteria->compare('password',$this->password,true);
		$criteria->compare('active',$this->active);
		$criteria->compare('role_id',$this->role_id);
		$criteria->compare('comment',$this->comment,true);
		$criteria->compare('date',$this->date,true);


		if(isset($this->kw)) {
			$criteria->addSearchCondition('id', $this->kw);
			$criteria->addSearchCondition('first_name', $this->kw, true, 'OR');
			$criteria->addSearchCondition('last_name', $this->kw, true, 'OR');
			$criteria->addSearchCondition('email',$this->kw,true, 'OR');
			$criteria->addSearchCondition('login',$this->kw,true, 'OR');
			$criteria->addSearchCondition('active',$this->kw,true, 'OR');
			$criteria->addSearchCondition('role_id',$this->kw,true, 'OR');
		}

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

    public function login()
    {
        if ($model = self::findByAttributes(array('login' => $this->login)))
        {
            if ($model->password !== $this->password)
            {
                $this->addError('password', 'Wrong login or password');
                return false;
            }
        }
        else {
            $this->addError('password', 'User not found');
            return false;
        }
        if ($model->active !== USER::ACTIVE_TRUE) {
            if($model->active == User::ACTIVE_BANNED)
            {
                $this->addError('password', 'You were banned');
                return false;
            }
            if($model->active == User::ACTIVE_ME)
            {
                $this->addError('password', 'Wait until your user will be approved');
                return false;
            }
            if($model->active == User::WAITING_FOR_EMAIL_ACTIVIZATION)
            {
                $this->addError('password', 'Check your email for activation url');
                return false;
            }
        }
        $identity = new UserIdentity($model);
        $identity->authenticate();
        Yii::app()->user->login($identity, 3600 * 24 * 365 * 5); // 5 лет
        return true;
    }


    public function getUserData($id = null)
    {
        if(isset($id))
            $model = User::model()->findByPk($id);
        else
            $model = User::model()->findByPk(Yii::app()->user->id);
        return $model;

    }

    public function regUser()
    {

					if(App::model()->getAutoSend()){
						if($this->approveMe()){
							if($this->save())
								return true;
						}
					}else{
						$this->active = self::ACTIVE_ME;
						if($this->save()){
						$model = new Message('new');
						$model->from_id = $this->id;
						$model->to_id = Message::TO_ADMINS_AND_MODERS;
						$model->message = 'New user '.$this->login;
						$model->type = Message::TYPE_NEW_USER;
						if($model->save())
						{
							Yii::app()->user->setFlash('success','Your user has been created successfully. Check your email.');
							return true;
						}
					}
					}

						return false;

    }



    public function getCustomersData()
    {
        $criteria = User::model()->getUserStatus(User::ROLE_CUSTOMER)->getDbCriteria();
        $models = User::model()->findAll($criteria);
        return $models;
    }

    public function getUserStatus($status)
    {
        $crit = $this->getDbCriteria();
        $crit->addColumnCondition(array(
            $this->getTableAlias() . '.role_id' => $status,
        ));
        return $this;
    }

    public function getRoleTitle(){
        if (Yii::app()->user->checkAccess(User::ROLE_CUSTOMER) && !Yii::app()->user->checkAccess(User::ROLE_MODERATOR) && !Yii::app()->user->checkAccess(User::ROLE_ADMIN)) {
            echo 'CUSTOMER';
        }
        if (Yii::app()->user->checkAccess(User::ROLE_MODERATOR) && !Yii::app()->user->checkAccess(User::ROLE_ADMIN)) {
            echo 'TECHNICIAN';
        }
        if (Yii::app()->user->checkAccess(User::ROLE_ADMIN)) {
            echo 'ADMINISTRATOR';
        }
    }

	public function sendMail($alt, $text)
	{
		$message = "Message sent!";
		$mail = Yii::app()->mailer;
		$mail->AddAddress($this->email, $this->first_name);
		$mail->IsHTML(true); // set email format to HTML
		$mail->Subject = "From my site";
		$mail->Body = $text;
		$mail->AltBody = $alt;
		if (!$mail->Send()) {
			$message = "Message could not be sent. <p>";
			$message = "Mailer Error: " . $mail->ErrorInfo;
			return false;
		}
		return true;
	}

	public function approveMe()
	{
		if ($this->active == self::ACTIVE_ME) {
			$this->active = self::WAITING_FOR_EMAIL_ACTIVIZATION;
			$this->save();
			return $this->sendMail(
				'Copy and past it ' . Yii::app()->controller->createAbsoluteUrl('/user/approveme?url=' . str_replace
					('=', '', base64_encode('aprove_email:1:' . $this->password . ':' .$this->email . ':' . $this->last_name . ':' . $this->first_name))),
					'Please, <a href="' . (Yii::app()->controller->createAbsoluteUrl('/user/approveme?url=' . str_replace
					('=', '', base64_encode('aprove_email:1:' . $this->password . ':' . $this->email . ':' . $this->last_name . ':' . $this->first_name)))) . '">Click here</a>');

		}
	}


	public function findByMail($email)
	{
		$cr = $this->getDbCriteria();
		$cr->addColumnCondition(array(
			$this->getTableAlias() . '.email' => $email,
		));
		return $this;
	}
	
	public function approveUrl($url)
	{
		$data = explode(':', base64_decode($url));
		if (count($data) != 6) {
			return false;
		}
		if ($data[0] != 'aprove_email' && $data[1] != '1') {
			return false;
		}
		$me = $this->findByMail($data[3])->find();
		/** @var User $me */
		$me->active = 1;
		return $me->save();
	}



	public function afterFind()
	{
		parent::afterFind();
		$this->oldstatus   = $this->active;
	}

	public function beforeSave()
	{
	if( $this->oldstatus ==  self::ACTIVE_ME || $this->oldstatus == self::WAITING_FOR_EMAIL_ACTIVIZATION){
		if($this->oldstatus!=$this->active){
			$mes = Message::model()->findAllByAttributes(array('from_id'=>$this->id,'type'=>Message::TYPE_NEW_USER,
				'new'=>Message::MESSAGE_NEW));
			foreach($mes as $m){
				$m->new = Message::MESSAGE_OLD;
				$m->save();
			}
		}
	}
	return parent::beforeSave();
	}

    /**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Users the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
