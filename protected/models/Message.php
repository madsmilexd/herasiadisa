<?php

/**
 * This is the model class for table "message".
 *
 * The followings are the available columns in table 'message':
 * @property integer $message_id
 * @property integer $from_id
 * @property integer $to_id
 * @property integer $repair_id
 * @property integer $new
 * @property string $message
 * @property integer $hidden
 * @property string $date
 *
 * The followings are the available model relations:
 * @property Repair $repair
 * @property Users $from
 * @property Users $to
 * @property Message $type
 */
class Message extends CActiveRecord
{
    const TO_ADMINS_AND_MODERS = -1;

	const TYPE_NEW_COMMENT = 0;
	const TYPE_REPAIR_STATUS_CHANGE = 1;
	const TYPE_NEW_USER = 2;
	const TYPE_NEW_REPAIR = 3;
	const TYPE_NEW_PARTS = 4;
	const TYPE_MESSAGE = 5;

	const MESSAGE_NEW = 1;
	const MESSAGE_OLD = 0;
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'message';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
//			array('from_id, to_id, message', 'required'),
			array('from_id, to_id, repair_id, new, hidden', 'numerical', 'integerOnly'=>true),
			array('date,message', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('message_id, from_id, to_id, repair_id,  new, message, hidden, date', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'repair' => array(self::BELONGS_TO, 'Repair', 'repair_id'),
			'from' => array(self::BELONGS_TO, 'User', 'from_id'),
			'to' => array(self::BELONGS_TO, 'User', 'to_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'message_id' => 'Message',
			'from_id' => 'From',
			'to_id' => 'To',
			'repair_id' => 'Repair',
			'message' => 'Message',
			'date' => 'Date',
            'hidden' => 'Hidden',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('message_id',$this->message_id);
		$criteria->compare('from_id',$this->from_id);
		$criteria->compare('to_id',$this->to_id);
		$criteria->compare('repair_id',$this->repair_id);
		$criteria->compare('message',$this->message,true);
		$criteria->compare('date',$this->date,true);
		$criteria->compare('hidden',$this->hidden);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}


    public function getMyMessages(){

        $cr = $this->getDbCriteria();
        $cr->addInCondition('type', array(self::TYPE_MESSAGE));
        $cr->addInCondition('from_id', array(Yii::app()->user->id),'OR');
        $cr->addInCondition('to_id', array(Yii::app()->user->id),'OR');
        return $this;
    }

    public function getToMeMessages()
    {
        $cr = $this->getDbCriteria();
        $cr->addInCondition('type', array(self::TYPE_MESSAGE));
        $cr->addInCondition('to_id', array(Yii::app()->user->id));
        return $this;
    }

	public function getMessageList(){
		$res1 = self::model()->findAllByAttributes(array('to_id'=>Yii::app()->user->id,'type'=>self::TYPE_MESSAGE));
		$res2 = self::model()->findAllByAttributes(array('from_id'=>Yii::app()->user->id,'type'=>self::TYPE_MESSAGE));
		$res = array_merge($res1,$res2);
		return $res;
	}
    public function getMyChatMessages($id){
        $model = Message::model()->with('from','to')->findAllByAttributes(array('to_id'=>$id,'from_id'=>Yii::app()->user->id));
        $model2 = Message::model()->with('from','to')->findAllByAttributes(array('to_id'=>Yii::app()->user->id,'from_id'=>$id));
        $messages = array_merge($model, $model2);
        function sort_by_tid( $a, $b )
        {
            if(  $a->message_id ==  $b->message_id ){ return 0 ; }
            return ($a->message_id < $b->message_id) ? -1 : 1;
        }
        usort($messages,'sort_by_tid');
//        die(var_dump($messages));
        return $messages;
//        die(var_dump($model->attributes));
//        $cr = $this->getDbCriteria();
//        $cr->addInCondition('type', array(self::TYPE_MESSAGE));
//        $cr->addInCondition('from_id', array(Yii::app()->user->id,$id),'OR');
//        $cr->addInCondition('to_id', array(Yii::app()->user->id,$id),'OR');
////        $cr->order = "message_id DESC"; for desc view of data
//        return $this;
    }
	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Message the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
