<?php

/**
 * This is the model class for table "repair".
 *
 * The followings are the available columns in table 'repair':
 * @property integer $repair_id
 * @property integer $brand_id
 * @property string $model
 * @property string $accessories
 * @property integer $send_method
 * @property string $old_service_pswd
 * @property string $tracking_number
 * @property string $shiping_number
 * @property string $damage
 * @property string $date
 * @property integer $client_id
 * @property integer $support_id
 * @property integer $status
 * @property double $price
 *
 * The followings are the available model relations:
 * @property Message[] $messages
 * @property SendMethod $sendMethod
 * @property Brand $brand
 * @property Users $client
 * @property Users $support
 */
class Repair extends CActiveRecord
{

	public $mixedSearch;
	public $ms;
	public $old_status;
	const STATUS_NEW_ORDER = 0;
	const STATUS_WAITING_FOR_DIAGNOSTIC = 1;
	const STATUS_WAITING_FOR_CUSTOMER_AGREEMENT = 2;
	const STATUS_SENT_FOR_REPAIRS = 3;
	const STATUS_WAITING_FOR_SPARE_PARTS = 4;
	const STATUS_SPARE_PARTS_DERIVED = 5;
	const STATUS_SUCCESSFULLY_COMPLETED = 6;
	const STATUS_FAIL = 7;
	const STATUS_CANCELED_BY_CUSTOMER = 8;
	const TYPE_REPAIR = 0;
	const TYPE_PARTS = 1;

	public $attachment;

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'repair';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('brand_id, model, accessories, send_method, damage, client_id', 'required', 'on' => 'newrepair'),
			array('damage,diagnostic', 'required', 'on' => 'newpart'),
			array('brand_id, send_method, client_id, support_id, status', 'numerical', 'integerOnly' => true),
			array('price', 'numerical'),
			array('model, accessories, old_service_pswd, tracking_number, shiping_number', 'length', 'max' => 255),
			array('date', 'unsafe'),
			array('diagnostic,attachment, damage,warehouse_id', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('repair_id, brand_id, model, accessories, send_method, old_service_pswd, tracking_number, shiping_number, damage, date, client_id, support_id, status, price', 'safe', 'on' => 'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'messages' => array(self::HAS_MANY, 'Message', 'repair_id'),
			'sendMethod' => array(self::BELONGS_TO, 'SendMethod', 'send_method'),
			'brand' => array(self::BELONGS_TO, 'Brand', 'brand_id'),
			'client' => array(self::BELONGS_TO, 'User', 'client_id'),
			'support' => array(self::BELONGS_TO, 'User', 'support_id'),
			'warehouse' => array(self::BELONGS_TO, 'Warehouse', 'warehouse_id'),
			'colors' => array(self::BELONGS_TO, 'Colors', 'status'),
		);
	}


	public function afterFind()
	{

		parent::afterFind();

		$this->old_status = $this->status;
		if ($this->price == 0.00)
			$this->price = 'not set yet';

		return $this;
	}

	public function afterSave()
	{
		parent::afterSave();

		if ($this->status != $this->old_status) {
			$notif = new Message();
			$notif->type = Message::TYPE_REPAIR_STATUS_CHANGE;
			$notif->repair_id = $this->repair_id;
			$notif->new = Message::MESSAGE_NEW;
			$notif->message = 'Status changed from '.Colors::getStatusLabel($this->old_status).' to '
				.Colors::getStatusLabel($this->status);

			$notif->from_id = Yii::app()->user->id;

			if (Yii::app()->user->checkAccess(User::ROLE_MODERATOR)||Yii::app()->user->checkAccess(User::ROLE_ADMIN))
				$notif->to_id = $this->client_id;
			else
				$notif->to_id = Message::TO_ADMINS_AND_MODERS;

			if(!$notif->save()){
				Yii::app()->user->setFlash('danger','Notification error');
			}
		}
	}



	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'repair_id' => 'Repair ID',
			'brand_id' => 'Brand',
			'model' => 'Model',
			'accessories' => 'Accessories',
			'send_method' => 'Send Method',
			'old_service_pswd' => 'Old Service Password',
			'tracking_number' => 'Tracking Number',
			'shiping_number' => 'Shiping Number',
			'damage' => 'Damage',
			'date' => 'Date',
			'client_id' => 'Client',
			'support_id' => 'Support',
			'status' => 'Status',
			'price' => 'Price',
			'type' => 'Type',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.
		$criteria = new CDbCriteria;

		$criteria->compare('repair_id', $this->repair_id);
		$criteria->compare('brand_id', $this->brand_id);
		$criteria->compare('model', $this->model, true);
		$criteria->compare('accessories', $this->accessories, true);
		$criteria->compare('send_method', $this->send_method);
		$criteria->compare('old_service_pswd', $this->old_service_pswd, true);
		$criteria->compare('tracking_number', $this->tracking_number, true);
		$criteria->compare('shiping_number', $this->shiping_number, true);
		$criteria->compare('damage', $this->damage, true);
		$criteria->compare('diagnostic', $this->diagnostic, true);
		$criteria->compare('date', $this->date, true);
		$criteria->compare('client_id', $this->client_id);
		$criteria->compare('support_id', $this->support_id);
		$criteria->compare('status', $this->status);
		$criteria->compare('price', $this->price);
		$criteria->compare('type', $this->type);


		if (isset($this->mixedSearch)) {
			$user = User::model()->findByAttributes(array('login' => $this->mixedSearch));


			$criteria->addSearchCondition('date', strtotime($this->ms), true, 'OR');
			$criteria->addSearchCondition('repair_id', $this->mixedSearch, true, 'OR');
			$criteria->addSearchCondition('brand_id', $this->mixedSearch, true, 'OR');
			$criteria->addSearchCondition('model', $this->mixedSearch, true, 'OR');
			$criteria->addSearchCondition('accessories', $this->mixedSearch, true, 'OR');
			$criteria->addSearchCondition('send_method', $this->mixedSearch, true, 'OR');
			$criteria->addSearchCondition('old_service_pswd', $this->mixedSearch, true, 'OR');
			$criteria->addSearchCondition('tracking_number', $this->mixedSearch, true, 'OR');
			$criteria->addSearchCondition('shiping_number', $this->mixedSearch, true, 'OR');
			$criteria->addSearchCondition('damage', $this->mixedSearch, true, 'OR');
			$criteria->addSearchCondition('diagnostic', $this->mixedSearch, true, 'OR');
			$criteria->addSearchCondition('client_id', ($user) ? $user->id : $this->mixedSearch, true, 'OR');
			$criteria->addSearchCondition('support_id', ($user) ? $user->id : $this->mixedSearch, true, 'OR');
			$criteria->addSearchCondition('status', $this->mixedSearch, true, 'OR');
			$criteria->addSearchCondition('price', $this->mixedSearch, true, 'OR');
			$criteria->addSearchCondition('type', 0); //repair only
		}


		if (isset($this->ms)) {
			$user = User::model()->findByAttributes(array('login' => $this->$ms));

			$criteria->addSearchCondition('date', strtotime($this->ms), true, 'OR');
			$criteria->addSearchCondition('repair_id', $this->ms, true, 'OR');
			$criteria->addSearchCondition('brand_id', $this->ms, true, 'OR');
			$criteria->addSearchCondition('model', $this->ms, true, 'OR');
			$criteria->addSearchCondition('accessories', $this->ms, true, 'OR');
			$criteria->addSearchCondition('send_method', $this->ms, true, 'OR');
			$criteria->addSearchCondition('old_service_pswd', $this->ms, true, 'OR');
			$criteria->addSearchCondition('tracking_number', $this->ms, true, 'OR');
			$criteria->addSearchCondition('shiping_number', $this->ms, true, 'OR');
			$criteria->addSearchCondition('damage', $this->ms, true, 'OR');
			$criteria->addSearchCondition('diagnostic', $this->ms, true, 'OR');
			$criteria->addSearchCondition('client_id', ($user) ? $user->id : $this->ms, true, 'OR');
			$criteria->addSearchCondition('support_id', ($user) ? $user->id : $this->ms, true, 'OR');
			$criteria->addSearchCondition('status', $this->ms, true, 'OR');
			$criteria->addSearchCondition('price', $this->ms, true, 'OR');
			$criteria->addSearchCondition('type', 1); //repair only
		}

		return new CActiveDataProvider($this, array(
			'criteria' => $criteria,
		));
	}

//    public function beforeValidate() {
//        if(parent::beforeValidate()) {
//            $this->client_id = Yii::app()->user->id;
//            return true;
//        }
//        return false;
//    }

//	public function beforeSave()
//	{
//		if(parent::beforeSave()){
//			$this->date = ;
//			return true;
//		}
//	}
	public function ownRepairs()
	{
		$cr = $this->getDbCriteria();
		$cr->addInCondition('client_id', array(Yii::app()->user->id));

		return $this;
	}


	public function byIds($ar)
	{
		$cr = $this->getDbCriteria();
		$cr->addInCondition('repair_id', $ar);

		return $this;
	}

	public function MyRepairsAndFreeRepairs($ar)
	{
		$cr = $this->getDbCriteria();
		$cr->addInCondition('support_id', array($ar, 'NULL'));

		return $this;
	}

	public function ownParts()
	{
		$cr = $this->getDbCriteria();
		$cr->addInCondition('client_id', array(Yii::app()->user->id));
		return $this;
	}

	public function historyRepairs()
	{
		/*
    const STATUS_NEW_ORDER = 0;
    const STATUS_WAITING_FOR_DIAGNOSTIC = 1;
    const STATUS_WAITING_FOR_CUSTOMER_AGREEMENT = 2;
    const STATUS_SENT_FOR_REPAIRS = 3;
    const STATUS_WAITING_FOR_SPARE_PARTS = 4;
    const STATUS_SPARE_PARTS_DERIVED = 5;
    const STATUS_SUCCESSFULLY_COMPLETED = 6;
    const STATUS_FAIL = 7;
    const STATUS_CANCELED_BY_CUSTOMER = 8;
		 */
		$cr = $this->getDbCriteria();
		$cr->addInCondition('type', array(Repair::TYPE_REPAIR));
//        $cr->addInCondition('status', array('6', '7', '8'));
		$cr->addInCondition('status', array(self::STATUS_SUCCESSFULLY_COMPLETED, self::STATUS_FAIL));
		return $this;
	}

	public function activeRepairs()
	{
		/*
const STATUS_NEW_ORDER = 0;
const STATUS_WAITING_FOR_DIAGNOSTIC = 1;
const STATUS_WAITING_FOR_CUSTOMER_AGREEMENT = 2;
const STATUS_SENT_FOR_REPAIRS = 3;
const STATUS_WAITING_FOR_SPARE_PARTS = 4;
const STATUS_SPARE_PARTS_DERIVED = 5;
const STATUS_SUCCESSFULLY_COMPLETED = 6;
const STATUS_FAIL = 7;
const STATUS_CANCELED_BY_CUSTOMER = 8;
 */
		$cr = $this->getDbCriteria();
		$cr->addInCondition('type', array(Repair::TYPE_REPAIR));
		$cr->addInCondition('status', array(self::STATUS_NEW_ORDER, self::STATUS_WAITING_FOR_DIAGNOSTIC,
			self::STATUS_WAITING_FOR_CUSTOMER_AGREEMENT, self::STATUS_SENT_FOR_REPAIRS,
			self::STATUS_WAITING_FOR_SPARE_PARTS, self::STATUS_SPARE_PARTS_DERIVED, self::STATUS_CANCELED_BY_CUSTOMER));

		return $this;
	}

	public function getDoneRepairs($id = null)
	{
		$model = Repair::model()->with('client')->findAllByAttributes(array('client_id' => $id, 'status' => 6));
		return $model;
	}

	public function getActiveRepairs($id = null)
	{
		$model = Repair::model()->with('client')->findAllByAttributes(array('client_id' => $id, 'status' => array(0, 1, 2, 3, 4, 5)));
		return $model;
	}

	public function historyParts()
	{
		$cr = $this->getDbCriteria();
		$cr->addInCondition('type', array(self::TYPE_PARTS));
		$cr->addInCondition('status', array(self::STATUS_SPARE_PARTS_DERIVED, self::STATUS_CANCELED_BY_CUSTOMER));
		return $this;
	}

	public function activeParts()
	{
		$cr = $this->getDbCriteria();
		$cr->addInCondition('type', array(self::TYPE_PARTS));
		$cr->addInCondition('status', array(self::STATUS_WAITING_FOR_CUSTOMER_AGREEMENT, self::STATUS_NEW_ORDER, self::STATUS_WAITING_FOR_SPARE_PARTS));
		return $this;
	}

	public static function getStatus($status)
	{
		$gs = Colors::model()->findByAttributes(array('status' => $status));
		return $gs->status;
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Repair the static model class
	 */
	public static function model($className = __CLASS__)
	{
		return parent::model($className);
	}


}
