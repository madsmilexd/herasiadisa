<?php

class RepairsController extends Controller
{
	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl',
			//'postOnly + delete',
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions' => array('new', 'details', 'deletecomment', 'editcomment', 'AddComment', 'NewPart', 'partsdetails'),
				'users' => array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions' => array('palette', 'update','AddHiddenComment'),
				'roles' => array(User::ROLE_ADMIN, User::ROLE_MODERATOR),
			),
			array('deny', // deny all users
				'users' => array('*'),
			),
		);
	}

	public function actionDetails($id)
	{
		$model = Repair::model()->with
			('client',
				'messages',
				'brand',
				'support', 'sendMethod')->findByAttributes(array('repair_id' => $id));
		if (!isset($model->sendMethod->name)) {
			$model->sendMethod = Sendmethod::model()->find();
		}
		if (isset($_POST['Repair']['warehouse_id']) && $model->warehouse_id !== $_POST['Repair']['warehouse_id'] && $model->warehouse_id !== null) {
			$warehouse = Warehouse::model()->findByAttributes(array('number' => $model->warehouse_id));
			if ($warehouse) {
				$warehouse->status = Warehouse::STATUS_EMPTY;
				$warehouse->save();
			}
		}
		if (Yii::app()->request->getPost('Repair')) {

//			die(var_dump(Yii::app()->request->getPost('Repair')));
			$model->attributes = Yii::app()->request->getPost('Repair');
			(!is_numeric($model->price)) ? $model->price = 0 : $model->price = $model->price;
			(!is_numeric($model->support_id)) ? $model->support_id = NULL : $model->support_id = $model->support_id;
			if ($model->save()) {

				if (isset($model->warehouse_id)) {
					$warehouse = Warehouse::model()->findByAttributes(array('number' => $model->warehouse_id));
					if ($warehouse) {
						$warehouse->status = Warehouse::STATUS_NOT_EMPTY;
						$warehouse->save();
					}
				}

				$mes = new Message();
				$mes->type = Message::TYPE_REPAIR_STATUS_CHANGE;
				if (Yii::app()->user->checkAccess(User::ROLE_CUSTOMER) && (!Yii::app()->user->checkAccess
							(User::ROLE_MODERATOR) && !Yii::app()->user->checkAccess(User::ROLE_ADMIN))
				) {
					$mes->from_id = Yii::app()->user->id;
					$mes->to_id = Message::TO_ADMINS_AND_MODERS;
				} else {
					$mes->from_id = Yii::app()->user->id;
					$mes->to_id = $model->client_id;
				}
				$mes->repair_id = $model->repair_id;
				$mes->new = Message::MESSAGE_NEW;
				$mes->message = 'Changed';
				$mes->save();

				Yii::app()->user->setFlash('success', 'All changes was added');
				$this->redirect(Yii::app()->createAbsoluteUrl('crepair/'));
			} else {
				die(var_dump($model->getErrors()));

			}
		}
		if ($model)
			$this->render('details', array('model' => $model));
		else {
			Yii::app()->user->setFlash('warning', 'Repair not found');
			$this->redirect(Yii::app()->createAbsoluteUrl('crepair/index'));
		}
	}

	public function actionPartsDetails($id)
	{
		$model = Repair::model()->with
			('client',
				'messages',
				'support')->findByAttributes(array('repair_id' => $id));
		if (Yii::app()->request->getPost('Repair')) {
			$model->attributes = Yii::app()->request->getPost('Repair');
			if ($model->save()) {
				$mes = new Message();
				$mes->type = Message::TYPE_REPAIR_STATUS_CHANGE;
				if (Yii::app()->user->checkAccess(User::ROLE_CUSTOMER) && (!Yii::app()->user->checkAccess
							(User::ROLE_MODERATOR) && !Yii::app()->user->checkAccess(User::ROLE_ADMIN))
				) {
					$mes->from_id = Yii::app()->user->id;
					$mes->to_id = Message::TO_ADMINS_AND_MODERS;
				} else {
					$mes->from_id = Yii::app()->user->id;
					$mes->to_id = $model->client_id;
				}
				$mes->repair_id = $model->repair_id;
				$mes->new = Message::MESSAGE_NEW;
				$mes->message = 'Changed';
				$mes->save();
				Yii::app()->user->setFlash('success', 'All changes was added');
				$this->redirect(Yii::app()->createAbsoluteUrl('crepair/activeparts/'));
			}
		}
		$this->render('partsdetails', array('model' => $model));
	}


	public function actionNew()
	{
		$model = new Repair('newrepair');
		if (Yii::app()->request->getPost('Repair')) {

			$model->attributes = Yii::app()->request->getPost('Repair');
			if (!isset($model->client_id)) {
				$model->client_id = Yii::app()->user->id;
			}
			if ($model->save()) {
				$notification = new Message();
				$notification->from_id = $model->client_id;
				$notification->repair_id = $model->repair_id;
				$notification->to_id = Message::TO_ADMINS_AND_MODERS;
				$notification->type = Message::TYPE_NEW_REPAIR;
				$notification->message = "New repair from " . $model->client->login;
				if ($notification->save()) {
					Yii::app()->user->setFlash('info', 'New repair was added successfully');
					$this->redirect(Yii::app()->createAbsoluteUrl('/repairs/details/' . $model->repair_id));
					return true;
				} else {
					$model->addError('Notification', 'Something went wrong, contact with support');
					$this->render('new', array('model' => $model));
					return false;
				}

				// form inputs are valid, do something here
				return true;
			}
		}
		$this->render('new', array('model' => $model));
	}

	public function actionNewPart()
	{
		$model = new Repair('newpart');
		if (Yii::app()->request->getPost('Repair')) {


			// NEVER USE $_POST[''] !!!
			// Use Yii::app()->request->getPost('')
			// Lesha would kill you for this :D

			$model->attributes = Yii::app()->request->getPost('Repair');
			$model->type = Repair::TYPE_PARTS;
			$attach = new Attachment();
			$attach->attachment = $model->attachment;

			if (!isset($model->client_id)) {
				$model->client_id = Yii::app()->user->id;
			}

			if ($model->save()) {
				$attach->part_id = $model->repair_id;
				$attach->save();
				$notification = new Message();
				$notification->from_id = $model->client_id;
				$notification->repair_id = $model->repair_id;
				$notification->to_id = Message::TO_ADMINS_AND_MODERS;
				$notification->type = Message::TYPE_NEW_PARTS;
				$notification->message = "new orders for parts from " . Yii::app()->user->login;
				if ($notification->save()) {
					Yii::app()->user->setFlash('info', 'New order for spare part was added successfully');
					$this->redirect(Yii::app()->createAbsoluteUrl('/repairs/partsdetails/' . $model->repair_id));
					return true;
				} else {
					$model->addError('Notification', 'Something went wrong, contact with support');
					$this->render('new', array('model' => $model));
					return false;
				}

				// form inputs are valid, do something here
				return true;
			}
		}
		$this->render('newpart', array('model' => $model));
	}

	public function actionPalette()
	{
		$model = new Colors('search');
		$model->unsetAttributes(); // clear any default values
		if (isset($_GET['Colors']))
			$model->attributes = $_GET['Colors'];

		$this->render('palette', array(
			'model' => $model,
		));
	}


	public function actionDeleteComment($id)
	{
		$model = Message::model()->findByPK($id);
		if ($model->delete()) {
			Yii::app()->user->setFlash('info', 'Comment was successfully deleted');
		}
		$this->redirect(Yii::app()->createAbsoluteUrl('/'));
	}

	public function actionEditComment($id)
	{
		$model = Message::model()->findByPK($id);
		if (Yii::app()->request->getPost('Message')) {
			$model->attributes = Yii::app()->request->getPost('Message');
			if ($model->save())
				Yii::app()->user->setFlash('info', 'Comment was updated successfully');
			$this->redirect(Yii::app()->createAbsoluteUrl('crepair'));
		}
		$this->render('editcomment', array('model' => $model, 'id' => $id));
	}

	public function actionAddComment($id)
	{
		/** @var Message $model */
		$model = new Message('add');
		if (Yii::app()->request->getPost('Message')) {
			$model->attributes = Yii::app()->request->getPost('Message');
			$model->repair_id = $id;
			$model->from_id = Yii::app()->user->id;
			if ($model->save()) {
				Yii::app()->user->setFlash('info', 'Your comment was added successfully');
				$this->redirect(Yii::app()->user->returnUrl);
				/** @var Repair $client */
				$client = Repair::model()->findByPk($id);
				$notif = new Message();

				if ($client->client_id == Yii::app()->user->id) {
					$notif->from_id = Yii::app()->user->id;
					$notif->to_id = Message::TO_ADMINS_AND_MODERS;
				} else {
					$notif->from_id = Yii::app()->user->id;
					$notif->to_id = $client->client_id;
				}

				$notif->type = Message::TYPE_NEW_COMMENT;
				$notif->new = Message::MESSAGE_NEW;
				$notif->save();
			} else {
				die(var_dump('some_error'));
			}
		}
		$this->render('editcomment', array('model' => $model, 'id' => $id));

	}

    public function actionAddHiddenComment($id)
    {
        /** @var Message $model */
        $model = new Message('add');
        $model->hidden = 1;
        if (Yii::app()->request->getPost('Message')) {
            $model->attributes = Yii::app()->request->getPost('Message');
            $model->repair_id = $id;
            $model->from_id = Yii::app()->user->id;
            if ($model->save()) {
                Yii::app()->user->setFlash('info', 'Your comment was added successfully');
                $this->redirect(Yii::app()->user->returnUrl);
                /** @var Repair $client */
                $client = Repair::model()->findByPk($id);
                $notif = new Message();

                if ($client->client_id == Yii::app()->user->id) {
                    $notif->from_id = Yii::app()->user->id;
                    $notif->to_id = Message::TO_ADMINS_AND_MODERS;
                } else {
                    $notif->from_id = Yii::app()->user->id;
                    $notif->to_id = $client->client_id;
                }

                $notif->type = Message::TYPE_NEW_COMMENT;
                $notif->new = Message::MESSAGE_NEW;
                $notif->save();
            } else {
                die(var_dump('some_error'));
            }
        }
        $this->render('editcomment', array('model' => $model, 'id' => $id));


    }


	public function actionUpdate($id)
	{ //color update
		$model = Colors::model()->findByPk($id);
		if (!$model) {
			Yii::app()->user->setFlash('warning', 'Color with this id not defined');
			$this->redirect(Yii::app()->createAbsoluteUrl('repairs/palette'));

		}else{
			if(isset($_POST['Colors']))
			{
				$model->attributes=$_POST['Colors'];
				$model->save();
			}

		}
		$this->render('colorupdates', array('model' => $model));
	}
}