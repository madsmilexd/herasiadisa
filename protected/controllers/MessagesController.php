<?php

class MessagesController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
//			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view','userlist'),
				'users'=>array('*'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','update','Messages','Chat'),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','delete'),
				'users'=>array('admin'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
//	public function actionView($id)
//	{
//		$this->render('view',array(
//			'model'=>$this->loadModel($id),
//		));
//	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new Message;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Message']))
		{
			$model->attributes=$_POST['Message'];
            $model->from_id = Yii::app()->user->id;
            $model->type = Message::TYPE_MESSAGE;
			if($model->save())
            {
                Yii::app()->user->setFlash('success','Your message was send');
                $this->redirect(Yii::app()->createAbsoluteUrl('messages'));
            }
		}
		if(Yii::app()->user->checkAccess(User::ROLE_MODERATOR))
			$users = User::model()->findAll();
		else
			$users = User::model()->findAllByAttributes(array('role_id'=>array(User::ROLE_ADMIN,User::ROLE_MODERATOR)));


		$this->render('create',array(
			'model'=>$model, 'users'=>$users,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Message']))
		{
			$model->attributes=$_POST['Message'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->message_id));
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
//	public function actionDelete($id)
//	{
//		$this->loadModel($id)->delete();
//
//		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
//		if(!isset($_GET['ajax']))
//			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
//	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
        $model= Message::model()->getMessageList();
        $this->render('admin',array(
            'model'=>$model,
        ));
	}

    public function actionMessages($id = null){
        $model = new Message('lol');
        $model->getMyMessages($id);
//        die(var_dump($model->attributes));
        $this->render('admin',array(
            'model'=>$model,
        ));
    }

    public function actionChat($id){
        $model = new Message('lol');
        $new = new Message('newMessage');
        if(isset($_POST['Message']))
        {
            $new->attributes = Yii::app()->request->getPost('Message');
            $new->from_id = Yii::app()->user->id;
            $new->to_id = $id;
            $new->type = Message::TYPE_MESSAGE;
			$new->new = Message::MESSAGE_NEW;
            if($new->save())
            {
				Yii::app()->user->setFlash('success','Your message was send');
				//$this->redirect(Yii::app()->createAbsoluteUrl('messages'));
            }
        }
        $model = $model->getMyChatMessages($id);

        $this->render('chat',array('model'=>$model,'model2'=>$new));
    }

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return Message the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=Message::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	public function  actionUserlist($name){
		/** @var User $model */
		$model = User::model()->findAllByAttributes(array('login'=>$name));
		if($model){
			echo $model[0]->login.';'.$model[0]->id;
		}else{
			return $name;
		}
	}
	/**
	 * Performs the AJAX validation.
	 * @param Message $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='message-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
