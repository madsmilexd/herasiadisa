<?php
class UserController extends Controller
{
    public function accessRules()
    {
        return array(
            array('allow',  // allow all users to perform 'index' and 'view' actions
                'actions'=>array('index','registration','Approveme'),
                'users'=>array('*'),
            ),
            array('allow', // allow authenticated user to perform 'create' and 'update' actions
                'actions'=>array('Profile','editUser','Logout'),
                'users'=>array('@'),
            ),

        );
    }

    public function actionIndex()
    {
        $model = new User('login');
        if(Yii::app()->user->isGuest)
        {
            if (Yii::app()->request->getPost('User'))
            {
                $model->attributes = Yii::app()->request->getPost('User');
                if($model->login())
                {
                    $this->redirect(Yii::app()->createAbsoluteUrl('user/profile'));
                }
            }
        $this->render('index',array('model'=>$model));
        }
        else $this->redirect(Yii::app()->createAbsoluteUrl(Yii::app()->controller->id.'/profile'));
    }




    public function actionRegistration()
    {
        $model = new User('registration');
        if (Yii::app()->request->getPost('User'))
        {
			$model->attributes = Yii::app()->request->getPost('User');

        if($model->regUser())
        {
            $this->redirect(Yii::app()->homeUrl);
        }
        }
        $this->render('registration',array('model'=>$model));
    }

    public function actionProfile($id = null){
        $model = new User('profile');
        $model = $model->getUserData($id);
        $this->render('profile',array('model'=>$model,'id'=>$id));
    }

    public function actionEdit($id = null){
        $model = User::model()->getUserData($id);
        $model->setScenario('editUser');

        if(Yii::app()->request->getPost('User'))
        {
            $model->attributes = Yii::app()->request->getPost('User');

                if($model->save())
            $this->redirect(Yii::app()->createAbsoluteUrl(Yii::app()->controller->id).'/profile/'.$model->id);
        }
        $this->render('profile',array('model'=>$model,'id'=>$id));
    }

	public function actionLogout()
    {
        Yii::app()->user->logout();
        $this->redirect(Yii::app()->createAbsoluteUrl('user/index'));
    }


    public function actionError()
    {
        if ($error = Yii::app()->errorHandler->error) {
            if (Yii::app()->request->isAjaxRequest)
                echo $error['message'];
            else
                $this->render('error', $error);
        }
    }

	public function actionApproveme($url)
	{
		//YXByb3ZlX2VtYWlsOjE6YmFvam5zZ2VAMTBtYWlsLm9yZzpiYW9qbnNnZUAxMG1haWwub3JnOmNmYWRhOmFkc2FkYQ
		/** @var User $model */
		$model = User::model();
		if($model->approveUrl($url)){
			Yii::app()->user->setFlash('success','You\'r approve your email. Now you can enter');
			$this->redirect(Yii::app()->createAbsoluteUrl('/'));
		}else{
			Yii::app()->user->setFlash('error','Something went wrong. please contact with admins');
			$this->redirect(Yii::app()->createAbsoluteUrl('/'));
		}
	}

}