<?php

class CrepairController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout = '//layouts/column2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
//			'postOnly + delete', // we only allow deletion via POST request
		);

	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow', // allow all users to perform 'index' and 'view' actions
				'actions' => array('index', 'history', 'activeparts', 'Create', 'historyparts', 'newrepair', 'newactiveparts'),
				'users' => array('@'),
			),
			array('allow',
				'actions' =>array('support'),
				'roles' => array(User::ROLE_MODERATOR),
			),
			array('deny', // deny all users
				'users' => array('*'),
			),
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view', array(
			'model' => $this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model = new Repair;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if (isset($_POST['Repair'])) {
			$model->attributes = $_POST['Repair'];
			if ($model->save()) {
				Yii::app()->user->setFlash('info', 'New repair was added successfully');
				$this->redirect(array('view', 'id' => $model->repair_id));
			}
		}

		$this->render('create', array(
			'model' => $model,
		));
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex($mpp = 10)
	{
		if(!Yii::app()->user->checkAccess(User::ROLE_ADMIN)&&!Yii::app()->user->checkAccess(User::ROLE_CUSTOMER)){
			$this->redirect(Yii::app()->createAbsoluteUrl('crepair/support'));
		}

		$model = new Repair('search');
		$model->unsetAttributes(); // clear any default values

		if (isset($_GET['inpSearch'])) {
			$model->mixedSearch = $_GET['inpSearch'];
		}
		if (isset($_GET['Repair']))
			$model->attributes = $_GET['Repair'];
		if (Yii::app()->user->checkAccess(User::ROLE_MODERATOR))
			$model->activeRepairs();
		else $model->ownRepairs()->activeRepairs();


		// если че удалить эту хуйню и во вьюшке в дата провайдере вернуть модель-серч
		$dataProvider = new CActiveDataProvider($model, array(
			'Pagination' => array(
				'PageSize' => $mpp,
			),
		));

		$this->render('index', array(
			'model' => $model, 'dp' => $dataProvider
		));
	}


	public function actionSupport($mpp = 10)
	{
		$model = new Repair('search');
		$model->unsetAttributes(); // clear any default values
		$model->MyRepairsAndFreeRepairs(Yii::app()->user->id);
		if (isset($_GET['inpSearch'])) {
			$model->mixedSearch = $_GET['inpSearch'];
		}
		if (isset($_GET['Repair']))
			$model->attributes = $_GET['Repair'];


		// если че удалить эту хуйню и во вьюшке в дата провайдере вернуть модель-серч
		$dataProvider = new CActiveDataProvider($model, array(
			'Pagination' => array(
				'PageSize' => $mpp,
			),
		));

		$this->render('index', array(
			'model' => $model
		));
	}


	public function actionNewrepair($mpp = 10)
	{
		$id = -1;
		(Yii::app()->user->checkAccess(User::ROLE_MODERATOR)) ? $id = -1 : $id = Yii::app()->user->id;
		$notif = Message::model()->findAllByAttributes(array('to_id' => $id, 'new' => Message::MESSAGE_NEW, 'type' => array(Message::TYPE_NEW_REPAIR, Message::TYPE_REPAIR_STATUS_CHANGE)));

		if ($notif) {
			$ids = Array();

			foreach ($notif as $n) {
				array_push($ids, $n->repair_id);
				$n->new = Message::MESSAGE_OLD;
				$n->save();
			}
			$model = new Repair('search');
			$model->unsetAttributes(); // clear any default values
			$model->byIds($ids);


			// если че удалить эту хуйню и во вьюшке в дата провайдере вернуть модель-серч
			$dataProvider = new CActiveDataProvider($model, array(
				'Pagination' => array(
					'PageSize' => $mpp,
				),
			));


			$this->render('index', array(
				'model' => $model, 'dp' => $dataProvider,
			));


		} else {
			Yii::app()->user->setFlash('Success', 'No new notifications');
			$this->redirect(Yii::app()->createAbsoluteUrl('crepair'));
		}
	}

	public function actionNewactiveparts()
	{
		$id = -1;
		(Yii::app()->user->checkAccess(User::ROLE_MODERATOR)) ? $id = -1 : $id = Yii::app()->user->id;
		$notif = Message::model()->findAllByAttributes(array('to_id' => $id, 'new' => Message::MESSAGE_NEW,
			'type' => array(Message::TYPE_NEW_PARTS, Message::TYPE_REPAIR_STATUS_CHANGE)));

		if ($notif) {
			$ids = Array();

			foreach ($notif as $n) {
				array_push($ids, $n->repair_id);
				$n->new = Message::MESSAGE_OLD;
				$n->save();
			}
			$model = new Repair('search');
			$model->unsetAttributes(); // clear any default values
			$model->byIds($ids);
			$this->render('index', array(
				'model' => $model,
			));
		} else {
			Yii::app()->user->setFlash('Success', 'No new notifications');
			$this->redirect(Yii::app()->createAbsoluteUrl('crepair'));
		}
	}

	public function actionHistory($mpp = 10)
	{
		$model = new Repair('search');
		$model->unsetAttributes(); // clear any default values
		if (isset($_GET['Repair']))
			$model->attributes = $_GET['Repair'];
		if (Yii::app()->user->checkAccess(User::ROLE_MODERATOR))
			$model->historyRepairs();
		else $model->ownRepairs()->historyRepairs();

		// если че удалить эту хуйню и во вьюшке в дата провайдере вернуть модель-серч
		$dataProvider = new CActiveDataProvider($model, array(
			'Pagination' => array(
				'PageSize' => $mpp,
			),
		));


		$this->render('index', array(
			'model' => $model, 'dp' => $dataProvider,
		));
	}

	public function actionActiveParts($mpp = 10)
	{
		$model = new Repair('search');
		$model->unsetAttributes(); // clear any default values
		if (isset($_GET['Repair']))
			$model->attributes = $_GET['Repair'];
		if (isset($_GET['inpSearch'])) {
			$model->ms = $_GET['inpSearch'];
		}
		if (Yii::app()->user->checkAccess(User::ROLE_MODERATOR))
			$model->activeParts();
		else $model->ownParts()->activeParts();


		// если че удалить эту хуйню и во вьюшке в дата провайдере вернуть модель-серч
		$dataProvider = new CActiveDataProvider($model, array(
			'Pagination' => array(
				'PageSize' => $mpp,
			),
		));


		$this->render('parts', array(
			'model' => $model, 'dp' => $dataProvider
		));
	}

	public function actionHistoryParts($mpp = 10)
	{
		$model = new Repair('search');
		$model->unsetAttributes(); // clear any default values
		if (isset($_GET['Repair']))
			$model->attributes = $_GET['Repair'];
		if (Yii::app()->user->checkAccess(User::ROLE_MODERATOR))
			$model->historyParts();
		else $model->ownParts()->historyParts();


		// если че удалить эту хуйню и во вьюшке в дата провайдере вернуть модель-серч
		$dataProvider = new CActiveDataProvider($model, array(
			'Pagination' => array(
				'PageSize' => $mpp,
			),
		));


		$this->render('parts', array(
			'model' => $model, 'dp' => $dataProvider
		));
	}

	/**
	 * Manages all models.
	 */


	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return Repair the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model = Repair::model()->findByPk($id);
		if ($model === null)
			throw new CHttpException(404, 'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param Repair $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if (isset($_POST['ajax']) && $_POST['ajax'] === 'repair-form') {
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
